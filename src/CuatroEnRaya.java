import java.util.Random;
import java.util.Scanner;

public class CuatroEnRaya {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";
    public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
    public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
    public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
    public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
    public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";

    public static void play() {
        Scanner sc = new Scanner(System.in);
        Random random = new Random();

        String presel = null;

        String[][] board;

        int selection = 0;

        String player[] = { "✕", "○" };

        int round = 0;

        String actualplayer = null;

        int size = 0;

        int menu = 0;

        int players = 0;

        // Bucle encargado de que el juego se repita a no ser que el usuario quiera
        // salir de él
        do {

            presel = null;

            selection = 0;

            round = 0;

            actualplayer = null;

            size = 0;

            players = 0;

            App.clearScreen();

            // Bucle que repite el menu hasta que el usuario no ha elegido o ha decidido
            // salir
            do {

                System.out.print(ANSI_CYAN);
                System.out.println("██╗  ██╗███████╗███╗   ██╗██████╗  █████╗ ██╗   ██╗ █████╗ ");
                System.out.println("██║  ██║██╔════╝████╗  ██║██╔══██╗██╔══██╗╚██╗ ██╔╝██╔══██╗");
                System.out.println("███████║█████╗  ██╔██╗ ██║██████╔╝███████║ ╚████╔╝ ███████║");
                System.out.println("╚════██║██╔══╝  ██║╚██╗██║██╔══██╗██╔══██║  ╚██╔╝  ██╔══██║");
                System.out.println("     ██║███████╗██║ ╚████║██║  ██║██║  ██║   ██║   ██║  ██║");
                System.out.println("     ╚═╝╚══════╝╚═╝  ╚═══╝╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝   ╚═╝  ╚═╝");

                // Dependiendo del menu este switch ejecutara el primero o el segundo

                switch (menu) {
                    case 0:
                        System.out.print(ANSI_BLACK + ANSI_CYAN_BACKGROUND);
					    System.out.println("");
					    System.out.println("█████  Jugadores  ██████");
                        System.out.println("╔══════════════════════╗");
                        System.out.println("║ 1 - 2 Jugadores      ║");
                        System.out.println("╠══════════════════════╣");
                        System.out.println("║ 2 - 1 Jugador VS AI  ║");
                        System.out.println("╠══════════════════════╣");
                        System.out.println("║ Q - Salir            ║");
                        System.out.println("╚══════════════════════╝");

                        System.out.print(ANSI_RESET + ANSI_YELLOW);
                        System.out.print("\n→ " + ANSI_RESET);

                        presel = sc.next();

                        switch (presel.toLowerCase()) {
                            case "1":
                                players = 2;
                                break;
                            case "2":
                                players = 1;
                                break;
                            case "q":
                                players = 2;
                                size = 4;
                                break;

                            default:
                                players = -1;
                                break;
                        }

                        if (players < 0) {
                            App.clearScreen();
                            App.error("Introduce una selección válida.");
                        } else if (players != 0) {
                            App.clearScreen();
                            menu = 1;
                        }
                        break;

                    case 1:
                        System.out.print(ANSI_BLACK + ANSI_CYAN_BACKGROUND);
					    System.out.println("");
					    System.out.println("█ Dificultad del Juego █");
                        System.out.println("╔══════════════════════╗");
                        System.out.println("║ 1 - Facil 4x4        ║");
                        System.out.println("╠══════════════════════╣");
                        System.out.println("║ 2 - Normal 6x6       ║");
                        System.out.println("╠══════════════════════╣");
                        System.out.println("║ 3 - Difícil 8x8      ║");
                        System.out.println("╠══════════════════════╣");
                        System.out.println("║ R - Volver           ║");
                        System.out.println("╚══════════════════════╝");

                        System.out.print(ANSI_RESET + ANSI_YELLOW);
                        System.out.print("\n→ " + ANSI_RESET);

                        presel = sc.next();

                        switch (presel.toLowerCase()) {
                            case "1":
                                size = 4;
                                break;
                            case "2":
                                size = 6;
                                break;
                            case "3":
                                size = 8;
                                break;
                            case "r":
                                size = 0;
                                App.clearScreen();
                                menu = 0;
                                break;

                            default:
                                size = -1;
                                break;
                        }

                        if (size < 0) {
                            App.clearScreen();
                            App.error("Introduce una selección válida.");
                        }
                        break;

                    default:

                        break;
                }

            } while (players <= 0 || size <= 0);

            // Se crea el tablero teniendo en cuenta la selección del jugador

            board = new String[size][size];

            if (!presel.toLowerCase().equals("q")) {
                App.clearScreen();

                printBoard(board, "-1");

                // Este bule se repite hasta que hay un ganador o hasta que ya no hay mas
                // casillas por rellenar (empate)

                // Cada vez que se repite este bucle ejecutamos el turno de un jugador
                do {

                    if (round % 2 == 0) {
                        actualplayer = player[0];
                    } else {
                        actualplayer = player[1];
                    }

                    round++;

                    try {

                        String preselection;

                        if (players == 2 || actualplayer == "✕") {
                            System.out.print("\n\nIntroduce una columna 🤔: ");
                            preselection = sc.next();
                        } else {
                            System.out.print("\n\nLa IA está eligiendo... 🤔 ");
                            Thread.sleep(2300);
                            preselection = random.nextInt(size) + "";
                        }

                        if (isNumeric(preselection)) {
                            selection = Integer.parseInt(preselection);
                        } else {
                            selection = -1;
                        }

                        if (selection < 0 || selection >= board.length) {
                            throw new Exception("Introduce una selección válida.");
                        } else if (!place(board, selection, actualplayer)) {
                            throw new Exception("No se puede colocar ahi.");
                        } else {
                            App.clearScreen();
                        }
                    } catch (Exception e) {
                        App.clearScreen();
                        App.error(e.getMessage());
                        App.warning("Es el turno de " + actualplayer + ".");
                        round--;
                    }

                    printBoard(board, "" + selection);

                } while (!winner(actualplayer, board) && round < size * size);
                App.clearScreen();

                printBoard(board, "" + selection);

                // Aqui comprovamos si hay un ganador o si se ha llenado el tablero y por ende
                // es empate

                if (round >= size * size) {
                    System.out.println("\n\nSe ha terminado la partida. (Empate)");
                }

                for (String p : player) {
                    if (winner(p, board)) {
                        System.out.println("\n\nHa ganado " + ANSI_GREEN_BACKGROUND + ANSI_BLACK + " " + p + " "
                                + ANSI_RESET + " 🥳🎉.");
                    }
                }

                System.out.println("");

                App.pause();

            }
        } while (!presel.toLowerCase().equals("q"));

    }

    // Función para comprovar si un string es un número

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /*
     * Esta funcion es la enargada de colocar la ficha en el tablero.
     * Parametros de entrada 
     * - Recibetablero en si con las fichas que ya estan colocadas. 
     * - Recibe la columna que ha elegido el jugador 
     * - Recibe la ficha del jugador para colocarla en el tablero 
     * Parametros de salida 
     * - Devuelve verdadero si la ficha se ha podido colocar con exito en esa columna 
     * - Devuelve falso si no se ha podido colocar la ficha
     */

    public static boolean place(String[][] board, int selection, String player) {
        boolean placed = false;
        for (int i = board.length - 1; i >= 0; i--) {
            if (board[i][selection] == " " && placed == false) {
                board[i][selection] = player;
                placed = true;
            }
        }

        return placed;
    }
    /*
     * Esta funcion es la encargada de dibujar el tablero.
     * Parametros de entrada 
     * - Recibe el tablero en si con las fichas que ya estan colocadas. 
     * - Recibe un string que posteriormente se utiliza para saber 
     *   que casilla ha elegido el jugador para mostrarlo una vez elija
     * Esta funcion no tiene parametros de salida
     */
    public static void printBoard(String[][] board, String highlight) {

        for (int x = 0; x < board.length; x++) {

            for (int y = 0; y < board.length; y++) {
                if (board[x][y] == null) {
                    board[x][y] = " ";
                }

                if (board[x][y].contains("h")) {
                    board[x][y] = board[x][y].substring(0, 1);
                    if (y != 0) {
                        System.out.print(ANSI_CYAN + board[x][y] + ANSI_RESET + "│");
                    } else {
                        System.out.print("│" + ANSI_CYAN + board[x][y] + ANSI_RESET + "│");
                    }
                } else {
                    if (y != 0) {
                        System.out.print(board[x][y] + "│");
                    } else {
                        System.out.print("│" + board[x][y] + "│");
                    }
                }

            }

            System.out.println();
        }

        for (int i = 0; i < board.length; i++) {

            if (Integer.parseInt(highlight) == i) {
                System.out.print(" " + ANSI_GREEN + i + ANSI_RESET);
            } else {
                System.out.print(" " + i);
            }

        }

    }
    /*
     * EEsta funcion se encarga de comprobar si un jugador ha ganado.
     * Parametros de entrada 
     * - El jugador que queremos comprobar si ha ganado. 
     * - El tablero de juego
     * Parametros de salida 
     * - Devuelve verdadero si dicho jugador ha ganado
     * - Devuelve falso si no ha ganado
     */
    public static boolean winner(String player, String[][] board) {
        // check for 4 across
        for (int row = 0; row < board.length; row++) {
            for (int col = 0; col < board[0].length - 3; col++) {
                if (board[row][col].equals(player) && board[row][col + 1].equals(player)
                        && board[row][col + 2].equals(player) && board[row][col + 3].equals(player)) {
                    board[row][col] += "h";
                    board[row][col + 1] += "h";
                    board[row][col + 2] += "h";
                    board[row][col + 3] += "h";
                    return true;
                }
            }
        }
        // check for 4 up and down
        for (int row = 0; row < board.length - 3; row++) {
            for (int col = 0; col < board[0].length; col++) {
                if (board[row][col].equals(player) && board[row + 1][col].equals(player)
                        && board[row + 2][col].equals(player) && board[row + 3][col].equals(player)) {
                    board[row][col] += "h";
                    board[row + 1][col] += "h";
                    board[row + 2][col] += "h";
                    board[row + 3][col] += "h";
                    return true;
                }
            }
        }
        // check upward diagonal
        for (int row = 3; row < board.length; row++) {
            for (int col = 0; col < board[0].length - 3; col++) {
                if (board[row][col].equals(player) && board[row - 1][col + 1].equals(player)
                        && board[row - 2][col + 2].equals(player) && board[row - 3][col + 3].equals(player)) {
                    board[row][col] += "h";
                    board[row - 1][col + 1] += "h";
                    board[row - 2][col + 2] += "h";
                    board[row - 3][col + 3] += "h";
                    return true;
                }
            }
        }
        // check downward diagonal
        for (int row = 0; row < board.length - 3; row++) {
            for (int col = 0; col < board[0].length - 3; col++) {
                if (board[row][col].equals(player) && board[row + 1][col + 1].equals(player)
                        && board[row + 2][col + 2].equals(player) && board[row + 3][col + 3].equals(player)) {
                    board[row][col] += "h";
                    board[row + 1][col + 1] += "h";
                    board[row + 2][col + 2] += "h";
                    board[row + 3][col + 3] += "h";
                    return true;
                }
            }
        }
        return false;
    }
}
