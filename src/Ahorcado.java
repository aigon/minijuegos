import java.util.Random;
import java.util.Scanner;

public class Ahorcado {
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_WHITE = "\u001B[37m";
	public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
	public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
	public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
	public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
	public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
	public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
	public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
	public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";

	public static int vidas = 7; // vidas iniciales

	public static char x = '0';
	public static char x1 = '0';

	public static Scanner sc = new Scanner(System.in);

	public static Random rnd = new Random();
	// ************************************************************************************
	// ** Nombre de la funcio: play
	// ** INTERFAZ DEL JUEGO (MENU)
	// ** Parámetros de entrada:
	// ** Parámetros de salida: exit();
	// ************************************************************************************

	public static void play() {
		App.clearScreen();
		System.out.print(ANSI_YELLOW);
		System.out.println("██╗      █████╗     ███████╗ ██████╗  ██████╗  █████╗ \r\n"
				+ "██║     ██╔══██╗    ██╔════╝██╔═══██╗██╔════╝ ██╔══██╗\r\n"
				+ "██║     ███████║    ███████╗██║   ██║██║  ███╗███████║\r\n"
				+ "██║     ██╔══██║    ╚════██║██║   ██║██║   ██║██╔══██║\r\n"
				+ "███████╗██║  ██║    ███████║╚██████╔╝╚██████╔╝██║  ██║\r\n"
				+ "╚══════╝╚═╝  ╚═╝    ╚══════╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═╝");
		System.out.print(ANSI_RESET + ANSI_YELLOW_BACKGROUND + ANSI_BLACK);
		try {

			Thread.sleep(1000);
			System.out.println("╔═══════════════════════════╗\r\n" + "║           MENU            ║\r\n"
					+ "╠═══════════════════════════╣\r\n" + "║ 1- UN JUGADOR             ║\r\n"
					+ "║ 2- JUGADOR CONTRA JUGADOR ║\r\n" + "║                           ║\r\n"
					+ "║ Q- SALIR                  ║\r\n" + "╚═══════════════════════════╝\r\n");
		} catch (InterruptedException e) {
		}

		System.out.print(ANSI_RESET + ANSI_YELLOW);
		System.out.print("→ " + ANSI_RESET);
		x = sc.next().charAt(0);

		if (x == '1') {
			mod();
		}
		if (x == '2') {
			sc.nextLine().toLowerCase();// limpiar el reader
			plyvsply();
		}

		if (x == 'Q' || x1 == 'q') {
			exit();
		}

	}

	// ------------------------------------------------------------------------------------
	// ************************************************************************************
	// ** Nombre de la funcion: mod
	// ** Aqui estan las modalidades de los diferentes juegos
	// ** Parámetros de entrada:mod(); plyvsply();
	// ** Parámetros de salida: exit();
	// ************************************************************************************
	public static void mod() {
		App.clearScreen();
		try {
			Thread.sleep(1000);
			System.out.println(ANSI_YELLOW_BACKGROUND + ANSI_BLACK);
			System.out.println("╔══════════════════════════╗\r\n" + "║        MODALIDADES       ║\r\n"
					+ "╠══════════════════════════╣\r\n" + "║ 1- MODO NORMAL           ║\r\n"
					+ "║ 2- MODO LEAGE OF LEGENDS ║\r\n" + "║ 3- RUSIAN MODE           ║\r\n"
					+ "║ Q- SALIR                 ║\r\n" + "╚══════════════════════════╝");

			System.out.print(ANSI_RESET);

		} catch (InterruptedException e) {
		}

		x1 = sc.next().charAt(0);

		if (x1 == '1') {
			norm();
		}
		if (x1 == '2') {
			lol();
		}
		if (x1 == '3') {
			rusia();
		}
		if (x1 == 'Q' || x1 == 'q') {
			exit();
		}
	}

	// ************************************************************************************
	// ** Nombre de la funcion: game
	// ** Esta funcion coje la palabra "string" y la transforma en letras "chars",
	// despues estan los bucles los cuales compruevan las palabras
	// ** Parámetros de entrada:norm(); lol(); rusia(); 
	// ** Parámetros de salida: oculto1(resp); gameover(); play();
	// ************************************************************************************
	public static void game(String palabras) {
		char user = ' ';
		boolean cont = false;// CONATADOR DE INTENTOS
		int A = 0; // variable de aciertos

		// PASAR UN STRING A UN ARRAY DE CHAR
		// para repartir la palabra letra por letra en el array de caracteres
		char[] partir = part(palabras);
		// para printar la palabra en caso de que pierdas
		char[] over = part(palabras);
		char[] resp = new char[partir.length];

		// printa las casillas de la palabra
		for (int i = 0; i < resp.length; i++) {
			resp[i] = '_';
		}
		App.clearScreen();
		// mientras no perdamos vidas o no acertemos llamara a la funcion oculto
		while (vidas != 0 && A != resp.length) {

			oculto1(resp);

			// el usuario introduce las palabras
			System.out.print("\nIntroduce una letra: ");
			user = sc.next().toLowerCase().charAt(0);

			// comparar las letras puestas por el usuario con las que hay en el array

			for (int i = 0; i < partir.length; i++) {
				if (partir[i] == user) {
					resp[i] = partir[i];
					partir[i] = ' ';
					A++;
				}

			}

			vidas--;// restador de vidas
			imagAhorcado();// enseña el dibujo
			App.clearScreen();
		}

		// en caso de ganar
		if (A == resp.length) {
			System.out.print("\nEnhorabuena!! la palabra es: ");
			oculto1(resp);
		}
		// en caso de perder
		else {
			System.out.print("\nVaya malo... La palabra era: ");
			// coje la copia de la palabra y la printa por pantalla
			for (int i = 0; i < over.length; i++) {
				System.out.print(over[i] + " ");
			}
			System.out.println("\n");
			gameover();

		}
		vidas = 7;
		do {
			System.out.println("\nQUIERES REPETIR (S/N): ");
			user = sc.next().toLowerCase().charAt(0);
			if (user == 's') {
				play();
				vidas = 7;
			}
		} while (user != 'n' && user != 's');

	}

	// ************************************************************************************
	// ** Nombre de la función: norm
	// ** simplemente las palabras que hay en la modalida normal
	// ** Parámetros de entrada:
	// ** Parámetros de salida: game(string palabras)
	// ************************************************************************************
	public static void norm() {
		String palabras[] = new String[11];
		palabras[0] = "azucar";
		palabras[1] = "huevo";
		palabras[2] = "herramienta";
		palabras[3] = "sandia ";
		palabras[4] = "puerta";
		palabras[5] = "libro";
		palabras[6] = "galleta";
		palabras[7] = "flores";
		palabras[8] = "filete";
		palabras[9] = "esmeralda";
		palabras[10] = "conejo";
		game(palabras[rnd.nextInt(11)]);
	}

	// ************************************************************************************
	// ** Nombre de la función: lol
	// ** simplemente las palabras que hay en la modalida normal
	// ** Parámetros de entrada:
	// ** Parámetros de salida:game(string palabras)
	// ************************************************************************************
	public static void lol() {
		String palabras[] = new String[7];
		palabras[0] = "leona";
		palabras[1] = "braum";
		palabras[2] = "mordekaiser";
		palabras[3] = "qiyana";
		palabras[4] = "aphelios";
		palabras[5] = "rumble";
		palabras[6] = "aatrox";
		game(palabras[rnd.nextInt(7)]);

	}
	// ************************************************************************************
	// ** Nombre de la función: rusia
	// ** simplemente las palabras que hay en la modalida normal
	// ** Parámetros de entrada:
	// ** Parámetros de salida:game(string palabras)
	// ************************************************************************************

	public static void rusia() {
		String palabras[] = new String[6];
		palabras[0] = "da";
		palabras[1] = "nyet";
		palabras[2] = "spa-sí-ba";
		palabras[3] = "pri-vyét";
		palabras[4] = "chernóbyl";
		palabras[5] = "putin";
		game(palabras[rnd.nextInt(6)]);
	}

	// ************************************************************************************
	// ** Nombre de la función: plyvsply
	// ** El primer jugador escibira una palabra y el segundo la tendra que adivinar
	// solo tiene siete intentos
	// ** Parámetros de entrada:
	// ** Parámetros de salida:game(string palabras)
	// ************************************************************************************
	public static void plyvsply() {
		String word = " ";
		char esp = ' ';
		App.clearScreen();
		System.out.println("Escriba la palabra que quiere que su contrincante adivine (real):");
		word = sc.nextLine().toLowerCase();

		do {

			// detecta los espacios para prohibir las palabras compuestas
			for (int a = 0; a < word.length(); a++) {

				if (word.charAt(a) == ' ') {

					System.out.println("NO VALEN PALABRAS COMPUESTAS");
					plyvsply();
				}
			}

		} while (esp == word.length());
		App.clearScreen();
		game(word);

	}

	// ************************************************************************************
	// ** Nombre de la función: oculto1
	// ** ES LA funcion para comprovar palabras
	// ** Parámetros de entrada:
	// ** Parámetros de salida:game(String palabras)
	// ************************************************************************************
	// array oculto para acertar la palabra
	public static void oculto1(char[] resp) {

		for (int i = 0; i < resp.length; i++) {
			System.out.print(resp[i] + " ");
		}

	}

	// ************************************************************************************
	// ** Nombre de la función: imgAhorcado
	// ** Una imagen de un hombre ahorcado...
	// ** Parámetros de entrada:
	// ** Parámetros de salida:game(String palabras)
	// ************************************************************************************
	// decoracion para cuando no aciertas
	public static void imagAhorcado() {
		if (vidas == 6) {
			System.out.print(ANSI_BLACK);
			System.out.println("      +---+\r\n" + "      |   |\r\n" + "          |\r\n" + "          |\r\n"
					+ "          |\r\n" + "          |\r\n" + "    =========");
			System.out.print(ANSI_RESET);
		}
		if (vidas == 5) {
			System.out.print(ANSI_GREEN);
			System.out.println("      +---+\r\n" + "      |   |\r\n" + "      O   |\r\n" + "          |\r\n"
					+ "          |\r\n" + "          |\r\n" + "    =========");
			System.out.print(ANSI_RESET);
		}
		if (vidas == 4) {
			System.out.print(ANSI_YELLOW);
			System.out.println("      +---+\r\n" + "      |   |\r\n" + "      O   |\r\n" + "      |   |\r\n"
					+ "          |\r\n" + "          |\r\n" + "    =========");
			System.out.print(ANSI_RESET);
		}
		if (vidas == 3) {
			System.out.print(ANSI_CYAN);
			System.out.println("      +---+\r\n" + "      |   |\r\n" + "      O   |\r\n" + "     /|   |\r\n"
					+ "          |\r\n" + "          |\r\n" + "    =========");
			System.out.print(ANSI_RESET);
		}
		if (vidas == 2) {
			System.out.print(ANSI_BLUE);
			System.out.println("      +---+\r\n" + "      |   |\r\n" + "      O   |\r\n" + "     /|\\  |\r\n"
					+ "          |\r\n" + "          |\r\n" + "    =========");
			System.out.print(ANSI_RESET);
		}
		if (vidas == 1) {
			System.out.print(ANSI_RED);
			System.out.println("      +---+\r\n" + "      |   |\r\n" + "      O   |\r\n" + "     /|\\  |\r\n"
					+ "     /    |\r\n" + "          |\r\n" + "    =========");
			System.out.print(ANSI_RESET);
		}
		if (vidas == 0) {
			System.out.println(ANSI_PURPLE);
			System.out.println("      +---+\r\n" + "      |   |\r\n" + "      O   |\r\n" + "     /|\\  |\r\n"
					+ "     / \\  |\r\n" + "          |\r\n" + "    =========");
			System.out.print(ANSI_RESET);
		}

	}
	// ************************************************************************************
	// ** Nombre de la función: part
	// ** Permite partir los strings
	// ** Parámetros de entrada:
	// ** Parámetros de salida:game(String palabras)
	// ************************************************************************************

	// separa el array escajido en casillas
	public static char[] part(String randpalabra) {
		char[] letras;
		letras = new char[randpalabra.length()];
		// PASSAR DE STRING A UN ARRAY DE CHAR PARA COMPROVAR LA LETRA PUESTA POR EL
		// usuario
		for (int i = 0; i < randpalabra.length(); i++) {
			letras[i] = randpalabra.charAt(i);
		}
		// DEVOLVEMOS LETRA POR LETRA
		return letras;
	}

	// -----------------------------------------------------------------------------------
	public static void gameover() {
		System.out.println(ANSI_RED);
		System.out.println("\n ___________.._______\r\n" + "| .__________))______|\r\n" + "| | / /      ||\r\n"
				+ "| |/ /       ||\r\n" + "| | /        ||.-''.\r\n" + "| |/         |/  _  \\\r\n"
				+ "| |          ||  `/,|\r\n" + "| |          (\\\\`_.'\r\n" + "| |         .-`--'.\r\n"
				+ "| |        /Y . . Y\\\r\n" + "| |       // |   | \\\\\r\n" + "| |      //  | . |  \\\\\r\n"
				+ "| |     ')   |   |   (`\r\n" + "| |          ||'||\r\n" + "| |          || ||\r\n"
				+ "| |          || ||\r\n" + "| |          || ||\r\n" + "| |         / | | \\\r\n"
				+ "\"\"\"\"\"\"\"\"\"\"|_`-' `-' |\"\"\"|\r\n" + "|\"|\"\"\"\"\"\"\"\\ \\       '\"|\"|\r\n"
				+ "| |        \\ \\        | |\r\n" + ": :         \\ \\       : :  \r\n"
				+ ". .          `'       . .");
		System.out.print(ANSI_RESET);
	}

	// -----------------------------------------------------------------------------------
	public static void exit() {
		play();
	}

}
