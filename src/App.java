import java.util.Scanner;

public class App {
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_WHITE = "\u001B[37m";
	public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
	public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
	public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
	public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
	public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
	public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
	public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
	public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";

	// Funcion para limpiar pantalla
	// Imprime por consola unos caracteres que la consola los interpreta como si
	// hicieramos un clear
	public static void clearScreen() {
		System.out.print("\033[H\033[2J");
		System.out.flush();
	}

	// Función princimal, esta función es la encargada de ejecutar todo el código
	public static void main(String[] args) throws Exception {
		Scanner reader = new Scanner(System.in);
		String select = null;
		int menu = 0;

		// Tenemos un dowhile que ejecuta el menu principal en bucle hasta que el
		// usuario decide detener el programa de esta forma se ejecutaran los juegos
		// independientemente y una vez el juego finalice el programa regresara al menu

		do {
			clearScreen();
			System.out.print(ANSI_RED);
			System.out.println("Developed & designed by");
			System.out.println(" █████╗ ██╗ ██████╗  ██████╗ ███╗   ██╗");
			System.out.println("██╔══██╗██║██╔════╝ ██╔═══██╗████╗  ██║");
			System.out.println("███████║██║██║  ███╗██║   ██║██╔██╗ ██║");
			System.out.println("██╔══██║██║██║   ██║██║   ██║██║╚██╗██║");
			System.out.println("██║  ██║██║╚██████╔╝╚██████╔╝██║ ╚████║");
			System.out.println("╚═╝  ╚═╝╚═╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═══╝ CLI GAMES INC.");
			System.out.print(ANSI_RESET);

			switch (menu) {
				case 0:
					System.out.print(ANSI_BLACK + ANSI_RED_BACKGROUND);
					System.out.println("");
					System.out.println("███████████  MENU  ████████████");
					System.out.println("╔═════════════════════════════╗");
					System.out.println("║          1 - Jugar          ║");
					System.out.println("╚═════════════════════════════╝");
					System.out.println("╔═════════════╗	╔═════════════╗");
					System.out.println("║ 2 - Autores ║	║ 3 - Info    ║");
					System.out.println("╚═════════════╝	╚═════════════╝");
					System.out.println("╔═════════════╗	╔═════════════╗");
					System.out.println("║ 4 - Version ║	║ Q - Salir   ║");
					System.out.println("╚═════════════╝	╚═════════════╝");
					System.out.print(ANSI_RESET + ANSI_YELLOW);
					System.out.print("\n→ " + ANSI_RESET);

					select = reader.next();

					switch (select) {
						case "1":
							menu = 1;
							break;
						case "2":
							menu = 2;
							break;
						case "3":
							menu = 3;
							break;
						case "4":
							menu = 4;
							break;
						default:
							break;
					}
					break;
				case 1:
					System.out.print(ANSI_BLACK + ANSI_RED_BACKGROUND);
					System.out.println("");
					System.out.println("██████████ Elige un Juego ██████████");
					System.out.println("╔══════════════════════════════════╗");
					System.out.println("║ 1 - Pokemon Josemi Playa Edition ║");
					System.out.println("╠══════════════════════════════════╣");
					System.out.println("║ 2 - Medieval Fighter             ║");
					System.out.println("╠══════════════════════════════════╣");
					System.out.println("║ 3 - La soga                      ║");
					System.out.println("╠══════════════════════════════════╣");
					System.out.println("║ 4 - Cuatro en raya               ║");
					System.out.println("╠══════════════════════════════════╣");
					System.out.println("║ R - Volver                       ║");
					System.out.println("╚══════════════════════════════════╝");
					System.out.print(ANSI_RESET + ANSI_YELLOW);
					System.out.print("\n→ " + ANSI_RESET);

					select = reader.next();
					// MENU

					switch (select.toLowerCase()) {
						case "1":
							pokemon.play();
							break;
						case "2":
							MedievalFighter.play();
							break;
						case "3":
							Ahorcado.play();
							break;
						case "4":
							presentGame("CuatroEnRaya");
							CuatroEnRaya.play();
							break;
						case "r":
							menu = 0;
							break;
						default:
							break;
					}
					break;
				case 2:
					System.out.print(ANSI_BLACK + ANSI_RED_BACKGROUND);
					System.out.println("");
					System.out.println("██████████  Autores  ██████████");
					System.out.println("╔═════════════════════════════╗");
					System.out.println("║ Jose Miguel Peinado         ║");
					System.out.println("║ Jordi Gómez                 ║");
					System.out.println("║ Klaudius Miskinis           ║");
					System.out.println("║ Eric Farre                  ║");
					System.out.println("╚═════════════════════════════╝");
					System.out.println("╔═════════════════════════════╗");
					System.out.println("║         R - Volver          ║");
					System.out.println("╚═════════════════════════════╝");
					System.out.print(ANSI_RESET + ANSI_YELLOW);
					System.out.print("\n→ " + ANSI_RESET);

					select = reader.next();
					// MENU

					switch (select.toLowerCase()) {
						case "r":
							menu = 0;
							break;
						default:
							break;
					}
					break;
				case 3:
					System.out.print(ANSI_BLACK + ANSI_RED_BACKGROUND);
					System.out.println("");
					System.out.println("██████████  Info  ██████████");
					System.out.println("════════════════════════════");
					System.out.print(ANSI_RESET + ANSI_RED);
					System.out.println(
							"Bienvenido, nos presentamos, somos AIGON y nos hemos encargado de tus videojuegos. Somos un grupo de programadores \naprendices que compartimos clase y nos hemos encargado de este proyecto. \n\nHemos desarrollado una interfaz en cli desde la cual puedes navegar entre los distintos juegos programados por nosotros, entre los cuales: \n\nListado de juegos incluidos \n- Cuatro en raya \n- La Soga \n- Pokemon Josemi Playa Edition \n- Medieval Fighter");
					System.out.print(ANSI_BLACK + ANSI_RED_BACKGROUND);
					System.out.println("════════════════════════════");
					System.out.println("╔══════════════════════════╗");
					System.out.println("║        R - Volver        ║");
					System.out.println("╚══════════════════════════╝");
					System.out.print(ANSI_RESET + ANSI_YELLOW);
					System.out.print("\n→ " + ANSI_RESET);

					select = reader.next();
					// MENU

					switch (select.toLowerCase()) {
						case "r":
							menu = 0;
							break;
						default:
							break;
					}
					break;
				case 4:
					System.out.print(ANSI_BLACK + ANSI_RED_BACKGROUND);
					System.out.println("");
					System.out.println("█████  Version 1.0.0  █████");
					System.out.println("╔═════════════════════════╗");
					System.out.println("║        R - Volver       ║");
					System.out.println("╚═════════════════════════╝");
					System.out.print(ANSI_RESET + ANSI_YELLOW);
					System.out.print("\n→ " + ANSI_RESET);

					select = reader.next();
					// MENU

					switch (select.toLowerCase()) {
						case "r":
							menu = 0;
							break;
						default:
							break;
					}
					break;
				default:
					break;
			}

		} while (!select.toLowerCase().equals("q"));

		reader.close();

	}

	// Función encargada de mostrar las normas de los juegos por pantalla

	public static void presentGame(String game) throws InterruptedException {

		clearScreen();

		if (game.equals("CuatroEnRaya")) {

			System.out.println("╔════════════════════════════════════════════════════╗");
			System.out.println("║                       Normas                       ║");
			System.out.println("╠════════════════════════════════════════════════════╣");
			System.out.println("║ Ambos jugadores tienen (una ficha por movimiento)  ║");
			System.out.println("║ en el tablero.                                     ║");
			System.out.println("╠════════════════════════════════════════════════════╣");
			System.out.println("║ Al colocarlas estas siempre 'caen hasta abajo'.    ║");
			System.out.println("╠════════════════════════════════════════════════════╣");
			System.out.println("║ La partida termina si una de las siguientes        ║");
			System.out.println("║ condiciones se cumple:                             ║");
			System.out.println("║   - Uno de los jugadores coloca cuatro             ║");
			System.out.println("║   o más fichas en una línea contínua vertical,     ║");
			System.out.println("║   horizontal o diagonalmente. Este jugador         ║");
			System.out.println("║   gana la partida.                                 ║");
			System.out.println("║   - Todas las casillas del tablero están           ║");
			System.out.println("║   ocupadas y ningún jugador cumple la              ║");
			System.out.println("║   condición anterior para ganar. En este           ║");
			System.out.println("║   caso la partida finaliza en empate.              ║");
			System.out.println("╚════════════════════════════════════════════════════╝");
			pause();
		}
	}

	// Función encargada de pausar la pantalla hasta que el usuario polse enter
	public static void pause() {
		Scanner reader = new Scanner(System.in);

		System.out.print("Pulsa INTRO para continuar.");

		// Pausa para que el usuario se lea las normas
		reader.nextLine();
	}

	// Funcion para printar errores
	public static void error(String message) {
		System.out.println(ANSI_RED_BACKGROUND + "Error: " + message + ANSI_RESET);
		System.out.println();
	}

	// Función para printar avisos
	public static void warning(String message) {
		System.out.println(ANSI_YELLOW_BACKGROUND + ANSI_BLACK + "Aviso: " + message + ANSI_RESET);
		System.out.println();
	}
}
