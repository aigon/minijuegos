import java.util.Random;
import java.util.Scanner;

public class MedievalFighter {

	// INICIALIZACION DE VARIABLES
	// JUG 1
	public static int vida = 0;
	public static int ataque = 0;
	public static int resistencia = 0;

	// JUG 2 O IA
	public static int vidaDos = 0;
	public static int ataqueDos = 0;
	public static int resistenciaDos = 0;

	// MUERTO
	public static boolean muerto = false;

	// JUEGO
	public static void play() {

		App.clearScreen();

		Scanner reader = new Scanner(System.in);
		Random aleatorio = new Random();

		// INICIALIZACION DE VARIABLES

		int eleccion[] = new int[10];
		// eleccion[0] = Jugador 1 ARMA para Ataque
		// eleccion[1] = Jugador 1 ARMADURA para Resistencia
		// eleccion[2]
		// eleccion[3] = Contra IA o contra JUGADOR
		// eleccion[4] = Jugador 2 ARMA para Ataque
		// eleccion[5] = Jugador 2 ARMADURA para Resistencia
		// eleccion[6] = eleccion de accion Jugador 1
		// eleccion[7] = Eleccion de accion Jugador 2
		// eleccion[8] = Eleccion de accion Jug2 (IA)
		// eleccion[9] = Salir o seguir

		// DIFICULTAD
		int dificultad = 0;
		boolean modo = false;
		boolean salir = false;

		do {
			// INICIALIZACION DE VARIABLES EN CASO DE QUE EL USUARIO QUIERA VOLVER A JUGAR
			vida = 0;
			ataque = 0;
			resistencia = 0;
			vidaDos = 0;
			ataqueDos = 0;
			resistenciaDos = 0;
			dificultad = 0;
			eleccion = new int[10];
			modo = false;

			titulo();

			do {
				infoUno(); // Dificultad del juego
				dificultad = reader.nextInt();
				if (dificultad == 4) {
					salir = true;
				}
			} while (dificultad < 1 || dificultad > 4);
			if (salir == false) {
				do {
					modo();// Modo IA o Jugador
					eleccion[3] = reader.nextInt();
					if (eleccion[3] == 0) {
						modo = false;
					} else {
						modo = true;
					}
				} while (eleccion[3] < 0 || eleccion[3] > 1);

				do {
					System.out.println("╔═══════════╗");
					System.out.println("║ JUGADOR 1 ║");
					System.out.println("╚═══════════╝");
					infoDos(); // Equipamiento de ataque JUG 1
					eleccion[0] = reader.nextInt();
				} while (eleccion[0] < 0 || eleccion[0] > 4);

				do {
					infoTres(); // Equipamiento de resistencia JUG 1
					eleccion[1] = reader.nextInt();
				} while (eleccion[1] < 0 || eleccion[1] > 3);

				vida = vida(dificultad);
				ataque = ataque(eleccion[0]);
				resistencia = resistencia(eleccion[1]);

				// JUGADOR 2
				if (modo == true) {
					do {
						System.out.println("╔═══════════╗");
						System.out.println("║ JUGADOR 2 ║");
						System.out.println("╚═══════════╝");
						infoDos(); // Equipamiento de ataque JUG 2
						eleccion[4] = reader.nextInt();
					} while (eleccion[4] < 0 || eleccion[4] > 4);

					do {
						infoTres(); // Equipamiento de resistencia JUG 2
						eleccion[5] = reader.nextInt();
					} while (eleccion[5] < 0 || eleccion[5] > 3);

					vidaDos = vida(dificultad);
					ataqueDos = ataque(eleccion[4]);
					resistenciaDos = resistencia(eleccion[5]);

				} else {
					// IA
					eleccion[4] = aleatorio.nextInt(5); // Equipamiento de ataque IA
					eleccion[5] = aleatorio.nextInt(4); // Equipamiento de resistencia IA
					vidaDos = vida(dificultad) + aleatorio.nextInt(15);
					ataqueDos = ataque(eleccion[4]);
					resistenciaDos = resistencia(eleccion[5]);
					esperar();
				}

				// EMPIEZA LA BATALLA
				do {
					do {
						turnoJugadorUno(); // Eleccion de accion Jug1
						eleccion[6] = reader.nextInt();
					} while (eleccion[6] < 1 || eleccion[6] > 4);

					accionJugUn(aleatorio.nextInt(50), eleccion[6], vidaDos, resistenciaDos, vida, resistencia, ataque,
							ataqueDos, muerto);
					infoParametrosJugUno(vida, resistencia, ataque);

					if (modo == true) {
						do {
							turnoJugadorDos(); // Eleccion de accion Jug2
							eleccion[7] = reader.nextInt();
						} while (eleccion[7] < 1 || eleccion[7] > 4);

						accionJugD(aleatorio.nextInt(50), eleccion[7], vidaDos, resistenciaDos, vida, resistencia,
								ataque, ataqueDos, muerto);
						infoParametrosJugDos(vidaDos, resistenciaDos, ataqueDos);

					} else if (modo == false) {
						turnoJugadorDos(); // Eleccion de accion Jug2
						eleccion[8] = aleatorio.nextInt(4) + 1;

						accionJugD(aleatorio.nextInt(50), eleccion[8], vidaDos, resistenciaDos, vida, resistencia,
								ataque, ataqueDos, muerto);
						if (muerto == false) {
							infoParametrosJugDos(vidaDos, resistenciaDos, ataqueDos);
						}
					}

					if (vida <= 0 || vidaDos <= 0) {
						muerto = true;
					}

				} while (muerto == false);

				if (vida >= 0) {
					System.out.println("La partida ha terminado, el jugador Dos gana.");
				} else if (vidaDos >= 0) {
					System.out.println("La partida ha terminado, el jugador Uno gana.");
				}

				do {
					System.out.println("¿Deseas continuar?");
					System.out.println("1 - Empezar otra vez");
					System.out.println("2 - Salir");
					eleccion[9] = reader.nextInt();
				} while (eleccion[9] < 1 || eleccion[9] > 2);

				if (eleccion[9] == 1) {
					salir = false;
				} else {
					salir = true;
				}
			}
		} while (salir == false);
	}

	// FUNCIONES

	// FUNCION infoParametrosJugUno
	// Se encarga de dar informacion del estado del usuario
	// PARAMETROS: vida, resistencia y ataque
	// SALIDA DE PARAMETROS: void
	public static void infoParametrosJugUno(int vid, int resista, int ataq) {
		System.out.println("\nEl J1 tiene " + vid + " de vida");
		System.out.println("El J1 tiene " + resista + " de resistencia");
		System.out.println("El J1 tiene " + ataq + " de ataque\n");
	}

	// FUNCION infoParametrosJugDos
	// Se encarga de dar informacion del estado del usuario
	// PARAMETROS: vida, resistencia y ataque
	// SALIDA DE PARAMETROS: void
	public static void infoParametrosJugDos(int vid, int resista, int ataq) {
		System.out.println("\nEl J2 tiene " + vid + " de vida");
		System.out.println("El J2 tiene " + resista + " de resistencia");
		System.out.println("El J2 tiene " + ataq + " de ataque\n");
	}

	// FUNCION accionJugD
	// La funcion se encarga de coger la probabilidad de suerte y dar una
	// bonificacion o penalizacion.
	// Tambien se encarga de ver la eleccion del usuario y decidir que hará. En este
	// caso se toma en cuenta la accion de ataque para quitar vida al otro usuario,
	// accion de defensa para cubrirnos, accion de
	// PARAMETROS: suerte(aleatorio), eleccion de accion, vidaJugadorDos,
	// resistenciaJugadorDos, vidaJugadorUno, resistenciaJugadorUno,
	// ataqueJugadorUno, ataqueJugadorDos
	// SALIDA DE PARAMETROS: void y todos los cambios que se realizan a las
	// variables se quedan guardadas fuera de la funcion.
	public static void accionJugD(int pro, int a, int vidaD, int resD, int vidaU, int resU, int aU, int aD, boolean m) {
		if (pro < 10) {
			System.out.println("J2: ¡Que suerte! Encuentras un platano, ganas 10 de vida");
			vidaD = vidaD + 10;
		} else if (pro < 20 && pro > 10) {
			System.out.println("J2: Te has tropezado, pierdes 10 de vida.");
			vidaD = vidaD - 10;
		}

		if (a == 1) {
			vidaU = vidaU - aD;
			System.out.println("J2: Has atacado y has quitado: " + aD + " puntos de vida.");
		} else if (a == 3) {
			vidaD = vidaD + 20;
			System.out.println("J2: Te has curado 20 de vida.");
		} else if (a == 2) {
			vidaD = vidaD + 10;
			resD = resD - 5;
			System.out.println("J2: Te defiendes y pierdes 7 de resistencia.");
		} else if (a == 4) {
			m = true;
			vidaD = 0;
			System.out.println("J2: Parece que has intentado huir. Te han cazado. Has perdido.");
		}
	}

	// FUNCION accionJugUn
	// La funcion se encarga de coger la probabilidad de suerte y dar una
	// bonificacion o penalizacion.
	// Tambien se encarga de ver la eleccion del usuario y decidir que hará. En este
	// caso se toma en cuenta la accion de ataque para quitar vida al otro usuario,
	// accion de defensa para cubrirnos, accion de
	// PARAMETROS: suerte(aleatorio), eleccion de accion, vidaJugadorDos,
	// resistenciaJugadorDos, vidaJugadorUno, resistenciaJugadorUno,
	// ataqueJugadorUno, ataqueJugadorDos
	// SALIDA DE PARAMETROS: void y todos los cambios que se realizan a las
	// variables se quedan guardadas fuera de la funcion.
	public static int accionJugUn(int pro, int a, int vidaD, int resD, int vidaU, int resU, int aU, int aD, boolean m) {
		if (pro < 10) {
			System.out.println("J1: ¡Que suerte! Encuentras una manzana y ganas 10 de vida.");
			vidaU = vidaU + 10;
		} else if (pro < 20 && pro > 10) {
			System.out.println("J1: Te has tropezado, pierdes 10 de vida.");
			vidaU = vidaU - 10;
		}

		if (a == 1) {
			vidaD = vidaD - aU;
			System.out.println("J1: Has atacado y has quitado: " + aU + " puntos de vida.");
			return vidaD;
		} else if (a == 3) {
			vidaU = vidaU + 20;
			System.out.println("J1: Te has curado 20 de vida.");
			return vidaU;
		} else if (a == 2) {
			vidaU = vidaU - 1;
			resU = resU - 7;
			System.out.println("J1: Te defiendes y pierdes 7 de resistencia.");
			return resU;
		} else if (a == 4) {
			m = true;
			vidaU = 0;
			System.out.println("J1: Parece que has intentado huir. Te han cazado. Has perdido.");
		}
		return vidaU;
	}

	// FUNCION vida
	// Se encarga de darle un valor a la variable vida que como maximo sera 150.
	// Dependiendo del modo que ha escogido el jugador.
	// PARAMETROS: eleccion de dificultad
	// SALIDA DE PARAMETROS: int para darle el valor a vida.
	public static int vida(int d) {
		int v = 0;
		if (d == 1) {
			v = 150;
		} else if (d == 2) {
			v = 125;
		} else if (d == 3) {
			v = 100;
		} else if (d == 4) {
			v = 75;
		}
		return v;
	}

	// FUNCION ataque
	// Se encarga de darle un valor a la variable ataque.
	// PARAMETROS: ataque
	// SALIDA DE PARAMETROS: int para darle el valor a ataque.
	public static int ataque(int a) {
		int ataque = 0;
		if (a == 0) {
			ataque = 20;
		} else if (a == 1) {
			ataque = 30;
		} else if (a == 3) {
			ataque = 15;
		} else if (a == 4) {
			ataque = 35;
		}
		return ataque;
	}

	// FUNCION resistencia
	// Se encarga de darle un valor a la variable resistencia
	// PARAMETROS: ataque
	// SALIDA DE PARAMETROS: int para darle el valor a resistencia.
	public static int resistencia(int r) {
		int resistencia = 0;
		if (r == 0) {
			if (r == 0) {
				resistencia = 30;
			} else if (r == 1) {
				resistencia = 50;
			} else if (r == 3) {
				resistencia = 25;
			} else if (r == 4) {
				resistencia = 31;
			}
		}
		return resistencia;
	}

	// FUNCION esperar
	// Se encarga de dar un margen de tiempo para que la IA parezca que esta
	// escogiendo
	// PARAMETROS: ninguno
	// SALIDA DE PARAMETROS: void.
	public static void esperar() {
		System.out.println("╔═══════════════════════╗");
		System.out.println("║ LA IA ESTA ESCOGIENDO ║");
		System.out.println("╚═══════════════════════╝");
		try {
			Thread.sleep(1500);
		} catch (InterruptedException e) {
		}
	}

	// FUNCION infoDos
	// Se encarga de darle hacer syso al usuario/jugador2
	// PARAMETROS: ninguno
	// SALIDA DE PARAMETROS: void
	public static void infoDos() {
		System.out.println("╔════════════════════════╗");
		System.out.println("║ ESCOGE TU EQUIPAMIENTO ║");
		System.out.println("╠════════════════════════╣");
		System.out.println("║ 0 - ARCO               ║");
		System.out.println("║ 1 - ESPADA             ║");
		System.out.println("║ 2 - LANZA              ║");
		System.out.println("║ 3 - BALLESTA           ║");
		System.out.println("║ 4 - MAZO               ║");
		System.out.println("╚════════════════════════╝");
	}

	// FUNCION infoTres
	// Se encarga de darle hacer syso al usuario/jugador2
	// PARAMETROS: ninguno
	// SALIDA DE PARAMETROS: void
	public static void infoTres() {
		System.out.println("╔════════════════════════╗");
		System.out.println("║ ESCOGE TU EQUIPAMIENTO ║");
		System.out.println("╠════════════════════════╣");
		System.out.println("║ 0 - ARMADURA LIGERA    ║");
		System.out.println("║ 1 - ARMADURA PESADA    ║");
		System.out.println("║ 2 - ESCUDO GRANDE      ║");
		System.out.println("║ 3 - ESCUDO PEQUEÑO     ║");
		System.out.println("╚════════════════════════╝");
	}

	// FUNCION infoUno
	// Se encarga de darle hacer syso al usuario/jugador2
	// PARAMETROS: ninguno
	// SALIDA DE PARAMETROS: void
	public static void infoUno() {
		System.out.println("╔══════════════════════╗");
		System.out.println("║ DIFICULTAD DEL JUEGO ║");
		System.out.println("╠══════════════════════╣");
		System.out.println("║ 1 - FACIL            ║");
		System.out.println("║ 2 - NORMAL           ║");
		System.out.println("║ 3 - DIFICIL          ║");
		System.out.println("║ 4 - SALIR            ║");
		System.out.println("╚══════════════════════╝");
	}

	// FUNCION modo
	// Se encarga de darle hacer syso al usuario
	// PARAMETROS: ninguno
	// SALIDA DE PARAMETROS: void
	public static void modo() {
		System.out.println("╔══════════════════╗");
		System.out.println("║ SELECCIONAR MODO ║");
		System.out.println("╠══════════════════╣");
		System.out.println("║ 0 - IA           ║");
		System.out.println("║ 1 - JUGADOR      ║");
		System.out.println("╚══════════════════╝");
	}

	// FUNCION turnoJugadorUno
	// Se encarga de darle hacer syso al jugador 1
	// PARAMETROS: ninguno
	// SALIDA DE PARAMETROS: void
	public static void turnoJugadorUno() {
		System.out.println("╔═════════════════╗");
		System.out.println("║ TURNO JUGADOR 1 ║");
		System.out.println("╠═════════════════╣");
		System.out.println("║ 1 - ATACAR      ║");
		System.out.println("║ 2 - DEFENDER    ║");
		System.out.println("║ 3 - CURARTE     ║");
		System.out.println("║ 4 - RENDIRTE    ║");
		System.out.println("╚═════════════════╝");
	}

	// FUNCION turnoJugadorDos
	// Se encarga de darle hacer syso al jugador2
	// PARAMETROS: ninguno
	// SALIDA DE PARAMETROS: void
	public static void turnoJugadorDos() {
		System.out.println("╔═════════════════╗");
		System.out.println("║ TURNO JUGADOR 2 ║");
		System.out.println("╠═════════════════╣");
		System.out.println("║ 1 - ATACAR      ║");
		System.out.println("║ 2 - DEFENDER    ║");
		System.out.println("║ 3 - CURARTE     ║");
		System.out.println("║ 4 - RENDIRTE    ║");
		System.out.println("╚═════════════════╝");
	}

	// FUNCION titulo
	// Se enseñar el titulo con sysos
	// PARAMETROS: ninguno
	// SALIDA DE PARAMETROS: void
	public static void titulo() {
		System.out.println("\n");
		System.out.println("███████╗██╗ ██████╗ ██╗  ██╗████████╗███████╗██████╗ ");
		System.out.println("██╔════╝██║██╔════╝ ██║  ██║╚══██╔══╝██╔════╝██╔══██╗");
		System.out.println("█████╗  ██║██║  ███╗███████║   ██║   █████╗  ██████╔╝");
		System.out.println("██╔══╝  ██║██║   ██║██╔══██║   ██║   ██╔══╝  ██╔══██╗");
		System.out.println("██║     ██║╚██████╔╝██║  ██║   ██║   ███████╗██║  ██║");
		System.out.println("╚═╝     ╚═╝ ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚══════╝╚═╝  ╚═╝");
		System.out.println("\n");
	}

	public static void exit() {
		play();
	}

}
