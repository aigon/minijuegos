
import java.util.Scanner;
import java.util.Random;

public class pokemon {

	public static void play() { // Funcion donde se inicia el menu donde puedes escoger todos los modos.

		App.clearScreen();
		Scanner reader = new Scanner(System.in);
		int opcion = 0;
		try { // para que thread.sleep funcione
			System.out.println("Abre la consola todo lo que puedas");
			Thread.sleep(5000); // Hace una pausa en el terminal de 5s
		} catch (InterruptedException e) {
		}

		logo();
		System.out.println("\n			          Menu\n" + "		        	 -------\n\n"
				+ "			 1 -> Combate vs ia <- 1\n" + "			  2 -> Combate 1v1 <- 2\n"
				+ "			     3 -> Info <- 3\n" + "			     4 -> Exit <- 4");
		opcion = reader.nextInt();
		if (opcion == 1) {
			combatevsia();
		} // Recoge la rescuesta del usuario y le lleva a la funcion que ha elegido
		if (opcion == 2) {
			combate1v1();
		}
		if (opcion == 3) {
			info();
		}
		if (opcion == 4) {
			exit();
		}

	}

	public static void logo() { // Logo del juego
		try {
			System.out.println("                                  ,'\\");
			Thread.sleep(50);
			System.out.println("    _.----.        ____         ,'  _\\   ___    ___     ____");
			Thread.sleep(50);
			System.out.println("_,-'       `.     |    |  /`.   \\,-'    |   \\  /   |   |    \\  |`.");
			Thread.sleep(50);
			System.out.println("\\      __    \\    '-.  | /   `.  ___    |    \\/    |   '-.   \\ |  |");
			Thread.sleep(50);
			System.out.println(" \\.    \\ \\   |  __  |  |/    ,','_  `.  |          | __  |    \\|  |");
			Thread.sleep(50);
			System.out.println("   \\    \\/   /,' _`.|      ,' / / / /   |          ,' _`.|     |  |");
			Thread.sleep(50);
			System.out.println("    \\     ,-'/  /   \\    ,'   | \\/ / ,`.|         /  /   \\  |     |");
			Thread.sleep(50);
			System.out.println("     \\    \\ |   \\_/  |   `-.  \\    `'  /|  |    ||   \\_/  | |\\    |");
			Thread.sleep(50);
			System.out.println("      \\    \\ \\      /       `-.`.___,-' |  |\\  /| \\      /  | |   |");
			Thread.sleep(50);
			System.out.println("       \\    \\ `.__,'|  |`-._    `|      |__| \\/ |  `.__,'|  | |   |");
			Thread.sleep(50);
			System.out.println("        \\_.-'       |__|    `-._ |              '-.|     '-.| |   |");
			Thread.sleep(50);
			System.out.println("                                `'                            '-._|");
			Thread.sleep(50);
		} catch (InterruptedException e) {
		}
	}

	public static void combatevsia() { // funcio del modo vs ia
		Scanner reader = new Scanner(System.in);
		Random random = new Random();
		int equipo1 = 0;
		int equipo2 = 0;
		System.out.println(
				"Elige tu pokemon \n 1-> Charizard <-1\n 2-> Blastoise <-2\n 3-> Venusaur <-3\n 4-> Gengar <-4\n 5-> Snorlax <-5\n 6-> Dragonite <-6");
		equipo1 = reader.nextInt(); // Pide al usuario elegir su pokemon
		equipo2 = random.nextInt(6); // La ia coge un pokemon aleatorio
		if (equipo1 == 1 && equipo2 == 1) {
			charizardvscharizardia();
		} // Todos estos if sirven para llamar a la funcion que sea necesario segun los
			// pokemon elegidos.
		if (equipo1 == 1 && equipo2 == 2) {
			charizardvsblastoiseia();
		}
		if (equipo1 == 1 && equipo2 == 3) {
			charizardvsvenusauria();
		}
		if (equipo1 == 1 && equipo2 == 4) {
			charizardvsgengaria();
		}
		if (equipo1 == 1 && equipo2 == 5) {
			charizardvssnorlaxia();
		}
		if (equipo1 == 1 && equipo2 == 6) {
			charizardvsdragoniteia();
		}
		if (equipo1 == 2 && equipo2 == 1) {
			blastoisevscharizardia();
		}
		if (equipo1 == 2 && equipo2 == 2) {
			blastoisevsblastoiseia();
		}
		if (equipo1 == 2 && equipo2 == 3) {
			blastoisevsvenusauria();
		}
		if (equipo1 == 2 && equipo2 == 4) {
			blastoisevsgengaria();
		}
		if (equipo1 == 2 && equipo2 == 5) {
			blastoisevssnorlaxia();
		}
		if (equipo1 == 2 && equipo2 == 6) {
			blastoisevsdragoniteia();
		}
		if (equipo1 == 3 && equipo2 == 1) {
			venusaurvscharizardia();
		}
		if (equipo1 == 3 && equipo2 == 2) {
			venusaurvsblastoiseia();
		}
		if (equipo1 == 3 && equipo2 == 3) {
			venusaurvsvenusauria();
		}
		if (equipo1 == 3 && equipo2 == 5) {
			venusaurvssnorlaxia();
		}
		if (equipo1 == 3 && equipo2 == 6) {
			venusaurvsdragoniteia();
		}
		if (equipo1 == 3 && equipo2 == 4) {
			venusaurvsgengaria();
		}
		if (equipo1 == 4 && equipo2 == 1) {
			gengarvscharizardia();
		}
		if (equipo1 == 4 && equipo2 == 2) {
			gengarvsblastoiseia();
		}
		if (equipo1 == 4 && equipo2 == 3) {
			gengarvsvenusauria();
		}
		if (equipo1 == 4 && equipo2 == 4) {
			gengarvsgengaria();
		}
		if (equipo1 == 4 && equipo2 == 5) {
			gengarvssnorlaxia();
		}
		if (equipo1 == 4 && equipo2 == 6) {
			gengarvsdragoniteia();
		}
		if (equipo1 == 5 && equipo2 == 1) {
			snorlaxvscharizardia();
		}
		if (equipo1 == 5 && equipo2 == 2) {
			snorlaxvsblastoiseia();
		}
		if (equipo1 == 5 && equipo2 == 3) {
			snorlaxvsvenusauria();
		}
		if (equipo1 == 5 && equipo2 == 4) {
			snorlaxvsgengaria();
		}
		if (equipo1 == 5 && equipo2 == 5) {
			snorlaxvssnorlaxia();
		}
		if (equipo1 == 5 && equipo2 == 6) {
			snorlaxvsdragoniteia();
		}
		if (equipo1 == 6 && equipo2 == 1) {
			dragonitevscharizardia();
		}
		if (equipo1 == 6 && equipo2 == 2) {
			dragonitevsblastoiseia();
		}
		if (equipo1 == 6 && equipo2 == 3) {
			dragonitevsvenusauria();
		}
		if (equipo1 == 6 && equipo2 == 4) {
			dragonitevsgengaria();
		}
		if (equipo1 == 6 && equipo2 == 5) {
			dragonitevssnorlaxia();
		}
		if (equipo1 == 6 && equipo2 == 6) {
			dragonitevsdragoniteia();
		}
	}

	public static void combate1v1() { // Funcion del combate j1 vs j2
		Scanner reader = new Scanner(System.in);
		int equipo1 = 0;
		int equipo2 = 0;
		System.out.println(
				"Elige tu pokemon J1 \n 1-> Charizard <-1\n 2-> Blastoise <-2\n 3-> Venusaur <-3\n 4-> Gengar <-4\n 5-> Snorlax <-5\n 6-> Dragonite <-6");
		equipo1 = reader.nextInt();
		System.out.println(
				"Elige tu pokemon J2 \n 1-> Charizard <-1\n 2-> Blastoise <-2\n 3-> Venusaur <-3\n 4-> Gengar <-4\n 5-> Snorlax <-5\n 6-> Dragonite <-6");
		equipo2 = reader.nextInt(); // El jugador 1 y 2 eligen sus pokemons y se guardan en equipo1 y equipo2
									// respectivamente.
		if (equipo1 == 1 && equipo2 == 1) {
			charizardvscharizard();
		} // Estos if llaman a la funcion necesaria segun los pokemon elegidos por ambos
			// jugadores.
		if (equipo1 == 1 && equipo2 == 2) {
			charizardvsblastoise();
		}
		if (equipo1 == 1 && equipo2 == 3) {
			charizardvsvenusaur();
		}
		if (equipo1 == 1 && equipo2 == 4) {
			charizardvsgengar();
		}
		if (equipo1 == 1 && equipo2 == 5) {
			charizardvssnorlax();
		}
		if (equipo1 == 1 && equipo2 == 6) {
			charizardvsdragonite();
		}
		if (equipo1 == 2 && equipo2 == 1) {
			blastoisevscharizard();
		}
		if (equipo1 == 2 && equipo2 == 2) {
			blastoisevsblastoise();
		}
		if (equipo1 == 2 && equipo2 == 3) {
			blastoisevsvenusaur();
		}
		if (equipo1 == 2 && equipo2 == 4) {
			blastoisevsgengar();
		}
		if (equipo1 == 2 && equipo2 == 5) {
			blastoisevssnorlax();
		}
		if (equipo1 == 2 && equipo2 == 6) {
			blastoisevsdragonite();
		}
		if (equipo1 == 3 && equipo2 == 1) {
			venusaurvscharizard();
		}
		if (equipo1 == 3 && equipo2 == 2) {
			venusaurvsblastoise();
		}
		if (equipo1 == 3 && equipo2 == 3) {
			venusaurvsvenusaur();
		}
		if (equipo1 == 3 && equipo2 == 5) {
			venusaurvssnorlax();
		}
		if (equipo1 == 3 && equipo2 == 6) {
			venusaurvsdragonite();
		}
		if (equipo1 == 3 && equipo2 == 4) {
			venusaurvsgengar();
		}
		if (equipo1 == 4 && equipo2 == 1) {
			gengarvscharizard();
		}
		if (equipo1 == 4 && equipo2 == 2) {
			gengarvsblastoise();
		}
		if (equipo1 == 4 && equipo2 == 3) {
			gengarvsvenusaur();
		}
		if (equipo1 == 4 && equipo2 == 4) {
			gengarvsgengar();
		}
		if (equipo1 == 4 && equipo2 == 5) {
			gengarvssnorlax();
		}
		if (equipo1 == 4 && equipo2 == 6) {
			gengarvsdragonite();
		}
		if (equipo1 == 5 && equipo2 == 1) {
			snorlaxvscharizard();
		}
		if (equipo1 == 5 && equipo2 == 2) {
			snorlaxvsblastoise();
		}
		if (equipo1 == 5 && equipo2 == 3) {
			snorlaxvsvenusaur();
		}
		if (equipo1 == 5 && equipo2 == 4) {
			snorlaxvsgengar();
		}
		if (equipo1 == 5 && equipo2 == 5) {
			snorlaxvssnorlax();
		}
		if (equipo1 == 5 && equipo2 == 6) {
			snorlaxvsdragonite();
		}
		if (equipo1 == 6 && equipo2 == 1) {
			dragonitevscharizard();
		}
		if (equipo1 == 6 && equipo2 == 2) {
			dragonitevsblastoise();
		}
		if (equipo1 == 6 && equipo2 == 3) {
			dragonitevsvenusaur();
		}
		if (equipo1 == 6 && equipo2 == 4) {
			dragonitevsgengar();
		}
		if (equipo1 == 6 && equipo2 == 5) {
			dragonitevssnorlax();
		}
		if (equipo1 == 6 && equipo2 == 6) {
			dragonitevsdragonite();
		}
	}

	// Todas las funciones hasta charizardia(); son iguales encuanto has estructura,
	// solo cambian valores dependiendo de los pokemon, asi que no es necesario
	// comentarlas todas

	public static void charizardvscharizard() { // Esta funcion es el combate entre charizard y charizard
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 266; // Vida del pokemon del j1
		double vida2 = 266; // Vida del pokemon del j2
		while (contador == 0) { // While para que haya rondas infinitas hasta que una muera y le sume 1 al
								// contador
			char jugar = ' ';
			charizardsprite(); // El sprite del pokemon del j2
			charizardespaldasprite(); // El sprite del pokemon de j1
			System.out.println("J1: " + vida + " " + "J2: " + vida2); // Muestra la vida del los pokemon
			vida2 = vida2 - menucombatecharizardj1() * 1.4; // Crea el dano de sus ataques, dependiendo del ataque
															// elegido y el pokemon que lo realiza y se lo resta a sus
															// vidas.
			vida = vida - menucombatecharizardj1() * 1.4;// Tambien muestra la ia de los ataques de los pokemon
			if (vida2 <= 0) { // Si la vida de los pokemon llega a cero j1 gana
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o."); // Pregunta si quieres
																									// volver a jugar
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo."); // Si no escribes bien la
																							// letra te hace repetirlo
																							// hasta que lo hagas bien.
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				} // si eligen s vuelven a jugar
				if (jugar == 'n') {
				} // Si eligen n dejan de jugar
				if (jugar == 'o') {
					combatevsia();
				}
			} // Si le dan a o cambian a otro modo
			if (vida <= 0 && contador < 1) { // como el anterior solo que gana j2 y si el contador ya es 1, no se
												// mostrara, asi evitamos que ganen los dos
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void charizardvsblastoise() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 266;
		double vida2 = 362;
		while (contador == 0) {
			char jugar = ' ';
			blastoisesprite();
			charizardespaldasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombatecharizardj1() * 1.4;
			vida = vida - menucombateblastoisej1() * 1.1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void charizardvsvenusaur() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 266;
		double vida2 = 362;
		while (contador == 0) {
			char jugar = ' ';
			venusaursprite();
			charizardespaldasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombatecharizardj1() * 1.4;
			vida = vida - menucombatevenusaurj1() * 1.2;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void charizardvsgengar() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 266;
		double vida2 = 230;
		while (contador == 0) {
			char jugar = ' ';
			gengarsprite();
			charizardespaldasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida = vida - menucombategengarj1() * 1.5;
			vida2 = vida2 - menucombatecharizardj1() * 1.4;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void charizardvssnorlax() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 266;
		double vida2 = 430;
		while (contador == 0) {
			char jugar = ' ';
			snorlaxsprite();
			charizardespaldasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombatecharizardj1() * 1.4;
			vida = vida - menucombatesnorlaxj1() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void charizardvsdragonite() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 266;
		double vida2 = 292;
		while (contador == 0) {
			char jugar = ' ';
			dragonitesprite();
			charizardespaldasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombatecharizardj1() * 1.4;
			vida = vida - menucombatedragonitej1() * 1.30;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void blastoisevscharizard() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 362;
		double vida2 = 266;
		while (contador == 0) {
			char jugar = ' ';
			charizardsprite();
			blastoiseespaldasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida = vida - menucombatecharizardj1() * 2;
			vida2 = vida2 - menucombateblastoisej1() * 1.1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void blastoisevsblastoise() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 362;
		double vida2 = 362;
		while (contador == 0) {
			char jugar = ' ';
			blastoisesprite();
			blastoiseespaldasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombateblastoisej1() * 1.1;
			vida = vida - menucombateblastoisej1() * 1.1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void blastoisevsvenusaur() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 362;
		double vida2 = 310;
		while (contador == 0) {
			char jugar = ' ';
			venusaursprite();
			blastoiseespaldasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida = vida - menucombatevenusaurj1() * 1.2;
			vida2 = vida2 - menucombateblastoisej1() * 1.1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void blastoisevsgengar() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 362;
		double vida2 = 230;
		while (contador == 0) {
			char jugar = ' ';
			gengarsprite();
			blastoiseespaldasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida = vida - menucombategengarj1() * 1.5;
			vida2 = vida2 - menucombateblastoisej1() * 1.1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void blastoisevssnorlax() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 362;
		double vida2 = 430;
		while (contador == 0) {
			char jugar = ' ';
			snorlaxsprite();
			blastoiseespaldasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombateblastoisej1() * 1.1;
			vida = vida - menucombatesnorlaxj1() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void blastoisevsdragonite() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 362;
		double vida2 = 292;
		while (contador == 0) {
			char jugar = ' ';
			dragonitesprite();
			blastoiseespaldasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida = vida - menucombatedragonitej1() * 1.30;
			vida2 = vida2 - menucombateblastoisej1() * 1.1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void venusaurvscharizard() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 310;
		double vida2 = 266;
		while (contador == 0) {
			char jugar = ' ';
			charizardsprite();
			venusaurespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida = vida - menucombatecharizardj1() * 1.4;
			vida2 = vida2 - menucombatevenusaurj1() * 1.2;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void venusaurvsblastoise() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 310;
		double vida2 = 362;
		while (contador == 0) {
			char jugar = ' ';
			blastoisesprite();
			venusaurespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombatevenusaurj1() * 1.2;
			vida = vida - menucombateblastoisej1() * 1.1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void venusaurvsvenusaur() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 310;
		double vida2 = 310;
		while (contador == 0) {
			char jugar = ' ';
			venusaursprite();
			venusaurespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombatevenusaurj1() * 1.2;
			vida = vida - menucombatevenusaurj1() * 1.2;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void venusaurvsgengar() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 310;
		double vida2 = 230;
		while (contador == 0) {
			char jugar = ' ';
			gengarsprite();
			venusaurespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida = vida - menucombategengarj1() * 1.5;
			vida2 = vida2 - menucombatevenusaurj1() * 1.2;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void venusaurvssnorlax() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 310;
		double vida2 = 430;
		while (contador == 0) {
			char jugar = ' ';
			snorlaxsprite();
			venusaurespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombatevenusaurj1() * 1.2;
			vida = vida - menucombatesnorlaxj1() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void venusaurvsdragonite() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 310;
		double vida2 = 292;
		while (contador == 0) {
			char jugar = ' ';
			dragonitesprite();
			venusaurespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida = vida - menucombatedragonitej1() * 1.30;
			vida2 = vida2 - menucombatevenusaurj1() * 1.2;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void gengarvscharizard() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 230;
		double vida2 = 266;
		while (contador == 0) {
			char jugar = ' ';
			charizardsprite();
			gengarespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombategengarj1() * 1.5;
			vida = vida - menucombatecharizardj1() * 1.4;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void gengarvsblastoise() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 230;
		double vida2 = 362;
		while (contador == 0) {
			char jugar = ' ';
			blastoisesprite();
			gengarespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombategengarj1() * 1.5;
			vida = vida - menucombateblastoisej1() * 1.1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void gengarvsvenusaur() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 230;
		double vida2 = 310;
		while (contador == 0) {
			char jugar = ' ';
			venusaursprite();
			gengarespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombategengarj1() * 1.5;
			vida = vida - menucombatevenusaurj1() * 1.2;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void gengarvsgengar() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 230;
		double vida2 = 230;
		while (contador == 0) {
			char jugar = ' ';
			gengarsprite();
			gengarespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombategengarj1() * 1.5;
			vida = vida - menucombategengarj1() * 1.5;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void gengarvssnorlax() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 230;
		double vida2 = 430;
		while (contador == 0) {
			char jugar = ' ';
			snorlaxsprite();
			gengarespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombategengarj1() * 1.5;
			vida = vida - menucombatesnorlaxj1() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void gengarvsdragonite() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 230;
		double vida2 = 292;
		while (contador == 0) {
			char jugar = ' ';
			dragonitesprite();
			gengarespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombategengarj1() * 1.5;
			vida = vida - menucombatedragonitej1() * 1.3;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void snorlaxvscharizard() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 430;
		double vida2 = 266;
		while (contador == 0) {
			char jugar = ' ';
			charizardsprite();
			snorlaxespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida = vida - menucombatecharizardj1() * 1.4;
			vida2 = vida2 - menucombatesnorlaxj1() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void snorlaxvsblastoise() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 430;
		double vida2 = 362;
		while (contador == 0) {
			char jugar = ' ';
			blastoisesprite();
			snorlaxespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida = vida - menucombateblastoisej1() * 1.1;
			vida2 = vida2 - menucombatesnorlaxj1() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void snorlaxvsvenusaur() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 430;
		double vida2 = 310;
		while (contador == 0) {
			char jugar = ' ';
			venusaursprite();
			snorlaxespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida = vida - menucombatevenusaurj1() * 1.2;
			vida2 = vida2 - menucombatesnorlaxj1() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void snorlaxvsgengar() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 430;
		double vida2 = 230;
		while (contador == 0) {
			char jugar = ' ';
			gengarsprite();
			snorlaxespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida = vida - menucombategengarj1() * 1.5;
			vida2 = vida2 - menucombatesnorlaxj1() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void snorlaxvssnorlax() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 430;
		double vida2 = 430;
		while (contador == 0) {
			char jugar = ' ';
			snorlaxsprite();
			snorlaxespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombatesnorlaxj1() * 1;
			vida = vida - menucombatesnorlaxj1() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void snorlaxvsdragonite() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 430;
		double vida2 = 292;
		while (contador == 0) {
			char jugar = ' ';
			dragonitesprite();
			snorlaxespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida = vida - menucombatedragonitej1() * 1.3;
			vida2 = vida2 - menucombatesnorlaxj1() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void dragonitevscharizard() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 292;
		double vida2 = 266;
		while (contador == 0) {
			char jugar = ' ';
			charizardsprite();
			dragoniteespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida = vida - menucombatecharizardj1() * 1.4;
			vida2 = vida2 - menucombatedragonitej1() * 1.3;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void dragonitevsblastoise() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 292;
		double vida2 = 362;
		while (contador == 0) {
			char jugar = ' ';
			blastoisesprite();
			dragoniteespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombatedragonitej1() * 1.3;
			vida = vida - menucombateblastoisej1() * 1.1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void dragonitevsvenusaur() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 292;
		double vida2 = 310;
		while (contador == 0) {
			char jugar = ' ';
			venusaursprite();
			dragoniteespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombatedragonitej1() * 1.3;
			vida = vida - menucombatevenusaurj1() * 1.2;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void dragonitevsgengar() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 292;
		double vida2 = 230;
		while (contador == 0) {
			char jugar = ' ';
			gengarsprite();
			dragoniteespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida = vida - menucombategengarj1() * 1.5;
			vida2 = vida2 - menucombatedragonitej1() * 1.3;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void dragonitevssnorlax() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 292;
		double vida2 = 430;
		while (contador == 0) {
			char jugar = ' ';
			snorlaxsprite();
			dragoniteespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombatedragonitej1() * 1.3;
			vida = vida - menucombatesnorlaxj1() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	public static void dragonitevsdragonite() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 292;
		double vida2 = 292;
		while (contador == 0) {
			char jugar = ' ';
			dragonitesprite();
			dragoniteespladasprite();
			System.out.println("J1: " + vida + " " + "J2: " + vida2);
			vida2 = vida2 - menucombatedragonitej1() * 1.3;
			vida = vida - menucombatedragonitej1() * 1.3;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("Jugador 2 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combate1v1();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combatevsia();
				}
			}
		}
	}

	// Como las anteriores funciones, todas las pokemonvspokemonia(); son iguales
	// salvo que cambian algunos valores dependiendo del pokemon, asi que solo
	// comentare la primera para evitar rebundancia.

	public static void charizardvscharizardia() { // Esta funcion es el combate entre charizard y charizard ia
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 266; // La vida del pokemon de j1
		double vida2 = 266; // La vida del pokemon de la ia
		while (contador == 0) { // Bucle para que cuando alguien gane le sume 1 al contador y el bucle pare, asi
								// se crean rondas infinitas hasta que un pokemon quede derrotado.
			char jugar = ' ';
			charizardsprite(); // El sprite del pokemon de la ia
			charizardespaldasprite(); // El sprite del pokemon de j1
			System.out.println("J1: " + vida + " " + "IA: " + vida2); // Muestra la vida del pokemon de j1 y la ia
			vida2 = vida2 - menucombatecharizardj1() * 1.4; // crea el dano llamando a otra funcion y el dano depende
															// del ataque selecionado, luego se le resta a la vida del
															// pokemon.
			vida = vida - charizardia() * 1.4; // Tambien muestra la ui de los ataques de lo pokemon
			if (vida2 <= 0) { // Si la vida del pokemon de la ia es 0 o menos, osea a muerto, ocurre lo
								// siguiente
				contador++; // suma 1 al contador
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o."); // Le dice al usuario
																									// que quiere hacer
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo."); // Si el usuario no elige las
																							// opcines mencionadas se lo
																							// repite hasta que las
																							// elija.
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				} // Si elige s sigue con el mismo modo otra partida
				if (jugar == 'n') {
				} // Si elige n deja de jugar
				if (jugar == 'o') {
					combate1v1();
				}
			} // Si elige o cambia al modo j1 vs j2.
			if (vida <= 0 && contador < 1) { // Lo mismo que el otro if, pero si el contador ya es 1 no se ejecuta, para
												// evitar que gane j1 y la ia o viceversa.
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void charizardvsblastoiseia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 266;
		double vida2 = 362;
		while (contador != 1) {
			char jugar = ' ';
			blastoisesprite();
			charizardespaldasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombatecharizardj1() * 1.4;
			vida = vida - blastoiseia() * 1.1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void charizardvsvenusauria() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 266;
		double vida2 = 362;
		while (contador == 0) {
			char jugar = ' ';
			venusaursprite();
			charizardespaldasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombatecharizardj1() * 1.4;
			vida = vida - venusauria() * 1.2;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void charizardvsgengaria() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 266;
		double vida2 = 230;
		while (contador == 0) {
			char jugar = ' ';
			gengarsprite();
			charizardespaldasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida = vida - gengaria() * 1.5;
			vida2 = vida2 - menucombatecharizardj1() * 1.4;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void charizardvssnorlaxia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 266;
		double vida2 = 430;
		while (contador == 0) {
			char jugar = ' ';
			snorlaxsprite();
			charizardespaldasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombatecharizardj1() * 1.4;
			vida = vida - snorlaxia() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void charizardvsdragoniteia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 266;
		double vida2 = 292;
		while (contador == 0) {
			char jugar = ' ';
			dragonitesprite();
			charizardespaldasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombatecharizardj1() * 1.4;
			vida = vida - dragoniteia() * 1.3;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void blastoisevscharizardia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 362;
		double vida2 = 266;
		while (contador == 0) {
			char jugar = ' ';
			charizardsprite();
			blastoiseespaldasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida = vida - charizardia() * 1.4;
			vida2 = vida2 - menucombateblastoisej1() * 1.1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void blastoisevsblastoiseia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 362;
		double vida2 = 362;
		while (contador == 0) {
			char jugar = ' ';
			blastoisesprite();
			blastoiseespaldasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombateblastoisej1() * 1.1;
			vida = vida - blastoiseia() * 1.1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void blastoisevsvenusauria() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 362;
		double vida2 = 310;
		while (contador == 0) {
			char jugar = ' ';
			venusaursprite();
			blastoiseespaldasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida = vida - venusauria() * 1.2;
			vida2 = vida2 - menucombateblastoisej1() * 1.1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void blastoisevsgengaria() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 362;
		double vida2 = 230;
		while (contador == 0) {
			char jugar = ' ';
			gengarsprite();
			blastoiseespaldasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida = vida - gengaria() * 1.5;
			vida2 = vida2 - menucombateblastoisej1() * 1.1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void blastoisevssnorlaxia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 362;
		double vida2 = 430;
		while (contador == 0) {
			char jugar = ' ';
			snorlaxsprite();
			blastoiseespaldasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombateblastoisej1() * 1.1;
			vida = vida - snorlaxia() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void blastoisevsdragoniteia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 362;
		double vida2 = 292;
		while (contador == 0) {
			char jugar = ' ';
			dragonitesprite();
			blastoiseespaldasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida = vida - dragoniteia() * 1.3;
			vida2 = vida2 - menucombateblastoisej1() * 1.1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void venusaurvscharizardia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 310;
		double vida2 = 266;
		while (contador == 0) {
			char jugar = ' ';
			charizardsprite();
			venusaurespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida = vida - charizardia() * 1.4;
			vida2 = vida2 - menucombatevenusaurj1() * 1.2;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void venusaurvsblastoiseia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 310;
		double vida2 = 362;
		while (contador == 0) {
			char jugar = ' ';
			blastoisesprite();
			venusaurespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombatevenusaurj1() * 1.2;
			vida = vida - blastoiseia() * 1.1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void venusaurvsvenusauria() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 310;
		double vida2 = 310;
		while (contador == 0) {
			char jugar = ' ';
			venusaursprite();
			venusaurespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombatevenusaurj1() * 1.2;
			vida = vida - venusauria() * 1.2;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void venusaurvsgengaria() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 310;
		double vida2 = 230;
		while (contador == 0) {
			char jugar = ' ';
			gengarsprite();
			venusaurespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida = vida - gengaria() * 1.5;
			vida2 = vida2 - menucombatevenusaurj1() * 1.2;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void venusaurvssnorlaxia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 310;
		double vida2 = 430;
		while (contador == 0) {
			char jugar = ' ';
			snorlaxsprite();
			venusaurespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombatevenusaurj1() * 1.2;
			vida = vida - snorlaxia() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void venusaurvsdragoniteia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 310;
		double vida2 = 292;
		while (contador == 0) {
			char jugar = ' ';
			dragonitesprite();
			venusaurespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida = vida - dragoniteia() * 1.3;
			vida2 = vida2 - menucombatevenusaurj1() * 1.2;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void gengarvscharizardia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 230;
		double vida2 = 266;
		while (contador == 0) {
			char jugar = ' ';
			charizardsprite();
			gengarespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombategengarj1() * 1.5;
			vida = vida - charizardia() * 1.4;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void gengarvsblastoiseia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 230;
		double vida2 = 362;
		while (contador == 0) {
			char jugar = ' ';
			blastoisesprite();
			gengarespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombategengarj1() * 1.5;
			vida = vida - blastoiseia() * 1.1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void gengarvsvenusauria() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 230;
		double vida2 = 310;
		while (contador == 0) {
			char jugar = ' ';
			venusaursprite();
			gengarespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombategengarj1() * 1.5;
			vida = vida - venusauria() * 1.2;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void gengarvsgengaria() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 230;
		double vida2 = 230;
		while (contador == 0) {
			char jugar = ' ';
			gengarsprite();
			gengarespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombategengarj1() * 1.5;
			vida = vida - gengaria() * 1.5;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void gengarvssnorlaxia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 230;
		double vida2 = 430;
		while (contador == 0) {
			char jugar = ' ';
			snorlaxsprite();
			gengarespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombategengarj1() * 1.5;
			vida = vida - snorlaxia() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void gengarvsdragoniteia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 230;
		double vida2 = 292;
		while (contador == 0) {
			char jugar = ' ';
			dragonitesprite();
			gengarespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombategengarj1() * 1.5;
			vida = vida - dragoniteia() * 1.3;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void snorlaxvscharizardia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 430;
		double vida2 = 266;
		while (contador == 0) {
			char jugar = ' ';
			charizardsprite();
			snorlaxespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida = vida - charizardia() * 1.4;
			vida2 = vida2 - menucombatesnorlaxj1() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void snorlaxvsblastoiseia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 430;
		double vida2 = 362;
		while (contador == 0) {
			char jugar = ' ';
			blastoisesprite();
			snorlaxespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida = vida - blastoiseia() * 1.1;
			vida2 = vida2 - menucombatesnorlaxj1() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void snorlaxvsvenusauria() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 430;
		double vida2 = 310;
		while (contador == 0) {
			char jugar = ' ';
			venusaursprite();
			snorlaxespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida = vida - venusauria() * 1.2;
			vida2 = vida2 - menucombatesnorlaxj1() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void snorlaxvsgengaria() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 430;
		double vida2 = 230;
		while (contador == 0) {
			char jugar = ' ';
			gengarsprite();
			snorlaxespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida = vida - gengaria() * 1.5;
			vida2 = vida2 - menucombatesnorlaxj1() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void snorlaxvssnorlaxia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 430;
		double vida2 = 430;
		while (contador == 0) {
			char jugar = ' ';
			snorlaxsprite();
			snorlaxespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombatesnorlaxj1() * 1;
			vida = vida - snorlaxia() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void snorlaxvsdragoniteia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 430;
		double vida2 = 292;
		while (contador == 0) {
			char jugar = ' ';
			dragonitesprite();
			snorlaxespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida = vida - dragoniteia() * 1.3;
			vida2 = vida2 - menucombatesnorlaxj1() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void dragonitevscharizardia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 292;
		double vida2 = 266;
		while (contador == 0) {
			char jugar = ' ';
			charizardsprite();
			dragoniteespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida = vida - charizardia() * 1.4;
			vida2 = vida2 - menucombatedragonitej1() * 1.3;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void dragonitevsblastoiseia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 292;
		double vida2 = 362;
		while (contador == 0) {
			char jugar = ' ';
			blastoisesprite();
			dragoniteespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombatedragonitej1() * 1.3;
			vida = vida - blastoiseia() * 1.1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void dragonitevsvenusauria() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 292;
		double vida2 = 310;
		while (contador == 0) {
			char jugar = ' ';
			venusaursprite();
			dragoniteespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombatedragonitej1() * 1.3;
			vida = vida - venusauria() * 1.2;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void dragonitevsgengaria() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 292;
		double vida2 = 230;
		while (contador == 0) {
			char jugar = ' ';
			gengarsprite();
			dragoniteespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida = vida - gengaria() * 1.5;
			vida2 = vida2 - menucombatedragonitej1() * 1.3;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void dragonitevssnorlaxia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 292;
		double vida2 = 430;
		while (contador == 0) {
			char jugar = ' ';
			snorlaxsprite();
			dragoniteespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombatedragonitej1() * 1.3;
			vida = vida - snorlaxia() * 1;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void dragonitevsdragoniteia() {
		Scanner reader = new Scanner(System.in);
		int contador = 0;
		double vida = 292;
		double vida2 = 292;
		while (contador == 0) {
			char jugar = ' ';
			dragonitesprite();
			dragoniteespladasprite();
			System.out.println("J1: " + vida + " " + "IA: " + vida2);
			vida2 = vida2 - menucombatedragonitej1() * 1.3;
			vida = vida - dragoniteia() * 1.3;
			if (vida2 <= 0) {
				contador++;
				System.out.println("Jugador 1 gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
			if (vida <= 0 && contador < 1) {
				contador++;
				System.out.println("IA gana, volver a jugar? s/n o cambiar de modo: o.");
				jugar = reader.next().charAt(0);
				while (jugar != 's' && jugar != 'n' && jugar != 'o') {
					System.out.println("Eso no es ni s, ni n, ni o vuelve a intentarlo.");
					jugar = reader.next().charAt(0);
				}
				if (jugar == 's') {
					combatevsia();
				}
				if (jugar == 'n') {
				}
				if (jugar == 'o') {
					combate1v1();
				}
			}
		}
	}

	public static void info() { // Funcion que muestra informacion sobre el juego
		Scanner reader = new Scanner(System.in);
		char modo = ' ';
		System.err.println("�Esto Explica como funciona el juego y informacion sobre el!"); // Muestra un texto en rojo
		System.out.println(
				"Modo combatevsia: En este modo se juega contra un ia, que elige un pokemon aleatorio y usa ataques aleatorios dentro de su set.");
		System.out.println(
				"Modo combate1v1: En este modo juegas contra otro jugador, si eres el primero en elegir eres el J1, y el otro jugador J2, va por turnos y los dos elegis los ataques que quereis usar,");
		System.out.println(
				"el pokemon que tenga prioridad sera el que mostrara la interfaz de cada jugador, por ejemplo si charizard es m�s rapido que blastoise cuando salgan los ataques saldran primero los de charizard.");
		System.out.println(
				"Informacion General: 1- Todos los pokemon tienen una velocidad, cuanto m�s alta, m�s prioridad de ataque tendr�.");
		System.out.println(
				"2-Snorlax siempre sera el ultimo en atacar, ya que es el m�s lento, excepto si ocurre lo que pone en el apartado 4.");
		System.out.println(
				"3-Gengar siempre ataca primero, ya que es el m�s rapido, excepto si ocurre lo que pone en el apartado 4.");
		System.out.println("4-Si los pokemon en combate son los mismos, el J1 tendr� prioridad.");
		System.out.println("Modo 1v1 -> A \nModo vs ia -> B\nSalir -> C");
		while (modo != 'A' && modo != 'B' && modo != 'C') {
			System.out.println("Eso no es ni A, ni B, ni C vuelve a intentarlo."); // Le da elegir al usuario 3
																					// opciones, si no elige las
																					// mostradas se lo hace repetir
																					// hasta que las elija.
			modo = reader.next().charAt(0);
		}
		if (modo == 'A') {
			combate1v1();
		} // Si elige A se va al modo j1 vs j2
		if (modo == 'B') {
			combatevsia();
		} // Si elige B se va al modo j1 vs ia
		if (modo == 'C') {
			exit();
		} // Si elige C se acaba el progama

	}

	public static void exit() { // Hace que se acabe el progama

	}

	public static void uiataquescharizard() { // La ui de los ataques de charizard
		String slot[] = ataquescharizard();

		System.out.println(" % /@@@@@@@@@@@@@@@@@@@@@@@@@@@@*# /@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&( /" + "");
		System.out.println(" &%                              %#                                          (# " + "");
		System.out.println(" &%                              %#                                          (# " + "");
		System.out.println(" &%                              %#      1-> " + slot[0] + " <-1               (# " + "");
		System.out.println(" &%                              %#      2-> " + slot[1] + " <-2               (# " + "");
		System.out.println(" &%                              %#      3-> " + slot[2] + " <-3               (# " + "");
		System.out.println(" &%                              %#      4-> " + slot[3] + " <-4               (# " + "");
		System.out.println(" &%                              %#                                          (# " + "");
		System.out.println("  *,                              //                                         (# " + "");
		System.out.println(" &.,&@@@@@@@@@@@@@@@@@@@@@@@@@@@*#.*&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&(.*" + "");
	}

	public static void uiataquesagua() { // La ui de los ataques de blastoise
		String slot[] = ataquestipoagua();

		System.out.println(" % /@@@@@@@@@@@@@@@@@@@@@@@@@@@@*# /@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&( /" + "");
		System.out.println(" &%                              %#                                          (# " + "");
		System.out.println(" &%                              %#                                          (# " + "");
		System.out.println(" &%                              %#      1-> " + slot[0] + " <-1               (# " + "");
		System.out.println(" &%                              %#      2-> " + slot[1] + " <-2               (# " + "");
		System.out.println(" &%                              %#      3-> " + slot[2] + " <-3               (# " + "");
		System.out.println(" &%                              %#      4-> " + slot[3] + " <-4               (# " + "");
		System.out.println(" &%                              %#                                          (# " + "");
		System.out.println("  *,                              //                                         (# " + "");
		System.out.println(" &.,&@@@@@@@@@@@@@@@@@@@@@@@@@@@*#.*&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&(.*" + "");
	}

	public static void uiataquesvenusaur() {// La ui de los ataques de venusaur
		String slot[] = ataquesvenusaur();

		System.out.println(" % /@@@@@@@@@@@@@@@@@@@@@@@@@@@@*# /@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&( /" + "");
		System.out.println(" &%                              %#                                          (# " + "");
		System.out.println(" &%                              %#                                          (# " + "");
		System.out.println(" &%                              %#      1-> " + slot[0] + " <-1               (# " + "");
		System.out.println(" &%                              %#      2-> " + slot[1] + " <-2               (# " + "");
		System.out.println(" &%                              %#      3-> " + slot[2] + " <-3               (# " + "");
		System.out.println(" &%                              %#      4-> " + slot[3] + " <-4               (# " + "");
		System.out.println(" &%                              %#                                          (# " + "");
		System.out.println("  *,                              //                                         (# " + "");
		System.out.println(" &.,&@@@@@@@@@@@@@@@@@@@@@@@@@@@*#.*&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&(.*" + "");
	}

	public static void uiataquesgengar() {// La ui de los ataques de gengar
		String slot[] = ataquesgengar();

		System.out.println(" % /@@@@@@@@@@@@@@@@@@@@@@@@@@@@*# /@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&( /" + "");
		System.out.println(" &%                              %#                                          (# " + "");
		System.out.println(" &%                              %#                                          (# " + "");
		System.out.println(" &%                              %#      1-> " + slot[0] + " <-1               (# " + "");
		System.out.println(" &%                              %#      2-> " + slot[1] + " <-2               (# " + "");
		System.out.println(" &%                              %#      3-> " + slot[2] + " <-3               (# " + "");
		System.out.println(" &%                              %#      4-> " + slot[3] + " <-4               (# " + "");
		System.out.println(" &%                              %#                                          (# " + "");
		System.out.println("  *,                              //                                         (# " + "");
		System.out.println(" &.,&@@@@@@@@@@@@@@@@@@@@@@@@@@@*#.*&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&(.*" + "");
	}

	public static void uiataquesnormal() {// La ui de los ataques de snorlax
		String slot[] = ataquestiponormal();

		System.out.println(" % /@@@@@@@@@@@@@@@@@@@@@@@@@@@@*# /@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&( /" + "");
		System.out.println(" &%                              %#                                          (# " + "");
		System.out.println(" &%                              %#                                          (# " + "");
		System.out.println(" &%                              %#      1-> " + slot[0] + " <-1               (# " + "");
		System.out.println(" &%                              %#      2-> " + slot[1] + " <-2               (# " + "");
		System.out.println(" &%                              %#      3-> " + slot[2] + " <-3               (# " + "");
		System.out.println(" &%                              %#      4-> " + slot[3] + " <-4               (# " + "");
		System.out.println(" &%                              %#                                          (# " + "");
		System.out.println("  *,                              //                                         (# " + "");
		System.out.println(" &.,&@@@@@@@@@@@@@@@@@@@@@@@@@@@*#.*&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&(.*" + "");
	}

	public static void uiataquesdragon() { // La ui de los ataques de dragonite
		String slot[] = ataquestipodragon();

		System.out.println(" % /@@@@@@@@@@@@@@@@@@@@@@@@@@@@*# /@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&( /" + "");
		System.out.println(" &%                              %#                                          (# " + "");
		System.out.println(" &%                              %#                                          (# " + "");
		System.out.println(" &%                              %#      1-> " + slot[0] + " <-1               (# " + "");
		System.out.println(" &%                              %#      2-> " + slot[1] + " <-2               (# " + "");
		System.out.println(" &%                              %#      3-> " + slot[2] + " <-3               (# " + "");
		System.out.println(" &%                              %#      4-> " + slot[3] + " <-4               (# " + "");
		System.out.println(" &%                              %#                                          (# " + "");
		System.out.println("  *,                              //                                         (# " + "");
		System.out.println(" &.,&@@@@@@@@@@@@@@@@@@@@@@@@@@@*#.*&@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&(.*" + "");
	}

	public static void ascuassprite() { // Sprite de ascuas
		try {
			System.err.println("                                      ...                                         ");
			Thread.sleep(20);
			System.err.println("                                     .....                                      " + "");
			Thread.sleep(20);
			System.err.println("                                     ........                                   " + "");
			Thread.sleep(20);
			System.err.println("                                    ............                                " + "");
			Thread.sleep(20);
			System.err.println("                                   ...............                              " + "");
			Thread.sleep(20);
			System.err.println("                                  ...................                           " + "");
			Thread.sleep(20);
			System.err.println("                                 .........#............                         " + "");
			Thread.sleep(20);
			System.err.println("                                ..........###............                       " + "");
			Thread.sleep(20);
			System.err.println("                              ..........######.............                     " + "");
			Thread.sleep(20);
			System.err.println("                            ..........##########............                    " + "");
			Thread.sleep(20);
			System.err.println("                          .........,############(............                   " + "");
			Thread.sleep(20);
			System.err.println("                       ..........################..............                 " + "");
			Thread.sleep(20);
			System.err.println("                     .........(###################.............                 " + "");
			Thread.sleep(20);
			System.err.println("                   ..........#####################..............                " + "");
			Thread.sleep(20);
			System.err.println("                  .........(#####################....##.........                " + "");
			Thread.sleep(20);
			System.err.println("                 ..........#####################(######*.........               " + "");
			Thread.sleep(20);
			System.err.println("                ..........###############...############........                " + "");
			Thread.sleep(20);
			System.err.println("                ..........############.......###########........                " + "");
			Thread.sleep(20);
			System.err.println("                 ..........#########.........###########.......                 " + "");
			Thread.sleep(20);
			System.err.println("                  ..........#######..........#..########......                  " + "");
			Thread.sleep(20);
			System.err.println("                    .........#####..............#######.....                    " + "");
			Thread.sleep(20);
			System.err.println("                      .........####.............#####......                     " + "");
			Thread.sleep(20);
			System.err.println("                         ........###...........####.....                        " + "");
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
	}

	public static void lanzallamassprite() { // Sprite de lanzallamas
		try {
			System.err.println("                      ,                                                         ");
			Thread.sleep(20);
			System.err.println("                 .,,,,,,                                                        " + "");
			Thread.sleep(20);
			System.err.println("            ..,*******.. .***.                                                  " + "");
			Thread.sleep(20);
			System.err.println("       .,,,,,,,,,,,**.      .**                                                 " + "");
			Thread.sleep(20);
			System.err.println("    ,,,...,,,*****,,,***....     .......                                        " + "");
			Thread.sleep(20);
			System.err.println(" ,,,....************,,,**********************,**                          / /  ." + "");
			Thread.sleep(20);
			System.err.println(",,...**********************************************                      ///// /" + "");
			Thread.sleep(20);
			System.err.println("...*************************************************                    ////////" + "");
			Thread.sleep(20);
			System.err.println(".*******...***.   .**********************************//.     * /    /////////// " + "");
			Thread.sleep(20);
			System.err.println("**.                 ..***********,,*******************////////    /**///////////" + "");
			Thread.sleep(20);
			System.err.println(".                     ....,,,,,,,,**********************/,,,,,///****///////////" + "");
			Thread.sleep(20);
			System.err.println("                         .....,,,,*.**.,,***************///*/,,******//////*//  " + "");
			Thread.sleep(20);
			System.err.println("                               ..         .,*****    .....,,,,,************/    " + "");
			Thread.sleep(20);
			System.err.println("                                  /,           ..    .....,,,..,,,,*///         " + "");
			Thread.sleep(20);
			System.err.println("                                      // / //*       .....,,,..,/,  .           " + "");
			Thread.sleep(20);
			System.err.println("                                                 ///*/ ///// .,                 " + "");
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
	}

	public static void pistolaaguasprite() {// El sprite de pistola agua
		try {
			System.out.println("                                                                 .,,,           " + "");
			Thread.sleep(20);
			System.out.println("                                        ,,                     ,,,,,,,,.        " + "");
			Thread.sleep(20);
			System.out.println("                                     .,,,,,,,                ,,..,,,,,,,,.      " + "");
			Thread.sleep(20);
			System.out.println("                                   ,,,,,,,,,,,,            ,,...,,,,,,,,,,,     " + "");
			Thread.sleep(20);
			System.out.println("                                 ,,,,,..,,,,,,,,,         ,....,,,,,,,,,,,,,.   " + "");
			Thread.sleep(20);
			System.out.println("                               ,,,,...,,,,,,,,,,,,,      ,,...,,,,,,,,,,,,,,,   " + "");
			Thread.sleep(20);
			System.out.println("                             ,,,,....,,,,,,,,,,,,,,,,    ,...,,,,,,,,,,,,,,,,,  " + "");
			Thread.sleep(20);
			System.out.println("                           ,,,,....,,,,,,,,,,,,,,,,,,,,  **,,,,,,,,,,,,,,,,,,   " + "");
			Thread.sleep(20);
			System.out.println("         ,,.             .,,,.....,,,,,,,,,,,,,,,,,,,,,,  ***,,,,,,,,,,,****    " + "");
			Thread.sleep(20);
			System.out.println("       ,,,,,,,          ,,,......,,,,,,,,,,,,,,,,,,,,,,,,,  ,************,      " + "");
			Thread.sleep(20);
			System.out.println("     ,,.,,,,,,,        ,,,......,,,,,,,,,,,,,,,,,,,,,,,,,,,                     " + "");
			Thread.sleep(20);
			System.out.println("    ,..,,,,,,,,,.     ,,,.......,,,,,,,,,,,,,,,,,,,,,,,,,,,.                    " + "");
			Thread.sleep(20);
			System.out.println("   ,...,,,,,,,,,,    .,,,......,,,,,,,,,,,,,,,,,,,,,,,,,,,,,                    " + "");
			Thread.sleep(20);
			System.out.println("   *.,,,,,,,,,,,,    ,,,,.....,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,.                   " + "");
			Thread.sleep(20);
			System.out.println("    **,,,,,,,,**.    **,.....,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,.                   " + "");
			Thread.sleep(20);
			System.out.println("      ********,      .**,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,                    " + "");
			Thread.sleep(20);
			System.out.println("                      ***,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,*.                    " + "");
			Thread.sleep(20);
			System.out.println("                       *****,,,,,,,,,,,,,,,,,,,,,,,,,,****.                     " + "");
			Thread.sleep(20);
			System.out.println("                         ******,,,,,,,,,,,,,,,,,,********                       " + "");
			Thread.sleep(20);
			System.out.println("                           ***********,,***************                         " + "");
			Thread.sleep(20);
			System.out.println("                              **********************                            " + "");
			Thread.sleep(20);
			System.out.println("                                   ,**********.                                 " + "");
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
	}

	public static void hidrobombasprite() { // Sprite de hidrobomba
		try {
			System.out.println("                                        (**,(                                   " + "");
			Thread.sleep(20);
			System.out.println("                                        (//((                                   " + "");
			Thread.sleep(20);
			System.out.println("                                                                    ,           " + "");
			Thread.sleep(20);
			System.out.println("               */         (,   ..              ...                  *.          " + "");
			Thread.sleep(20);
			System.out.println("       (.  .(               (                ,..    .,            #             " + "");
			Thread.sleep(20);
			System.out.println("            ...,*/             .            ,.               (*.     ...,*/     " + "");
			Thread.sleep(20);
			System.out.println("               ***(((/          *..(        .....,                (*,/          " + "");
			Thread.sleep(20);
			System.out.println("                **,**,,,.,    **.,,,        .....     (**(      (,*(            " + "");
			Thread.sleep(20);
			System.out.println("                *****,,..,**/,,...... ,   . ... ,    *,,/     (***(             " + "");
			Thread.sleep(20);
			System.out.println("                 /*,****,,,,,,,,,,,,( ......... .,,(*..*#., (*,**(              " + "");
			Thread.sleep(20);
			System.out.println("                   (/*,,,,..,,,,,,,,,,.... .... ..(,,,.,***,,**/(               " + "");
			Thread.sleep(20);
			System.out.println("                      (/*......,,,,,,*,,,.......(*,,,,,..,,**/(                 " + "");
			Thread.sleep(20);
			System.out.println("                        (/*.......,,,.,*/(#/#(/**,,,,,,,,,*/*                   " + "");
			Thread.sleep(20);
			System.out.println("                          (/.........,..,,,,,,,..,,,..,,//                      " + "");
			Thread.sleep(20);
			System.out.println("                           (/...,...............,,,,,,/(                        " + "");
			Thread.sleep(20);
			System.out.println("                           ((*..,,...,,........,**,,*/(                         " + "");
			Thread.sleep(20);
			System.out.println("                           ((*.,,*...,,,,.....,*/,,,/(                          " + "");
			Thread.sleep(20);
			System.out.println("                           ((*,*//,,,****,,..,*//*,,/(                          " + "");
			Thread.sleep(20);
			System.out.println("       ...,,,.         /(##((((#((///(((//*,,,*((((//((%                        " + "");
			Thread.sleep(20);
			System.out.println("      ....,,,,,,********///(%%###((######((//((##########(//////****,...        " + "");
			Thread.sleep(20);
			System.out.println("        ...,,,,,,********/********,,,.........,,,****************,,,,....       " + "");
			Thread.sleep(20);
			System.out.println("               ......,,,*****//////(((((((((((///**,,,...,,,,,*,,,,,...         " + "");
			Thread.sleep(20);
			System.out.println("                    ....,,,,****///((((((((((((///***,,,,,,.........            " + "");
			Thread.sleep(20);
			System.out.println("                         ....,,,,*****////////***,,,.........                   " + "");
			Thread.sleep(20);
			System.out.println("                               ......,,,,,,,,,....                              " + "");
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
	}

	public static void hojaagudasprite() { // Sprite de hoja afilada o hoja aguda
		try {
			System.out.println("                                           , . .                                " + "");
			Thread.sleep(20);
			System.out.println("                                //((((/((#(((((//(,*//*..                       " + "");
			Thread.sleep(20);
			System.out.println("                          .(/((###((##(####(###((##(#(((///*,                   " + "");
			Thread.sleep(20);
			System.out.println("                       #################(#%(##(##########((/(/*,                " + "");
			Thread.sleep(20);
			System.out.println("                   /#(#####%##/#######%#######(#%%##%##(####((#(((              " + "");
			Thread.sleep(20);
			System.out.println("                 %%%%###(##%%(%%#######%%###%##%%###%#%%#%###%######,           " + "");
			Thread.sleep(20);
			System.out.println("               %%%%%%%#%#%%%(%#%%#%%###%%###%###%%%%%%#%%#%%##%#######/         " + "");
			Thread.sleep(20);
			System.out.println("             (###%%#%%%#%%%%%%%#%%%%#%%#%#%%%%###(((/(,///((#(######/(((#((,    " + "");
			Thread.sleep(20);
			System.out.println("     /(##/,,,****,,,,,,*,,*,**///(((/(#/#(#(/####(####(##(####%(##(#####(       " + "");
			Thread.sleep(20);
			System.out.println("              %%%%%%%%%%%##%#######%#####/##(#*(###/##(####(###%###%((          " + "");
			Thread.sleep(20);
			System.out.println("                %%%%%%%%%%%####(###*###(#############(####((#%(#%###            " + "");
			Thread.sleep(20);
			System.out.println("                  %%%%%%%#%%%########(%(####%#(/#(##%####/(#/(#(#               " + "");
			Thread.sleep(20);
			System.out.println("                     &%%#%%#(#########(##%%%(##(###(#####(((#((                 " + "");
			Thread.sleep(20);
			System.out.println("                        %%###(#%##/#######%######(#####(####                    " + "");
			Thread.sleep(20);
			System.out.println("                            ####(#######(##(########%%%#                        " + "");
			Thread.sleep(20);
			System.out.println("                                  /%%%%%%%%%%%%#/,                              " + "");
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
	}

	public static void bombagermensprite() { // Sprite de bomba germen
		try {
			System.out.println("                  ,(  .(((((((((      *                                         " + "");
			Thread.sleep(20);
			System.out.println("                  ((( ((((((((////   ,                                          " + "");
			Thread.sleep(20);
			System.out.println("                    *.     *****////                                            " + "");
			Thread.sleep(20);
			System.out.println("                   ***************/((     ( (  ( / ** *  * .,  ,                " + "");
			Thread.sleep(20);
			System.out.println("                  *****************/(                                           " + "");
			Thread.sleep(20);
			System.out.println("                   (************/**((                                           " + "");
			Thread.sleep(20);
			System.out.println("                    (********//***(                                             " + "");
			Thread.sleep(20);
			System.out.println("                       ((**,,,(((                                               " + "");
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
	}

	public static void lenguetazosprite() { // Sprite de lenguetazo
		try {
			System.out.println("                     @/@     @/(                                                " + "");
			Thread.sleep(20);
			System.out.println("             @  #/@   @/@    @(         @/@                                     " + "");
			Thread.sleep(20);
			System.out.println("             /@  /     @///@//  %//@    //                                      " + "");
			Thread.sleep(20);
			System.out.println("              @/%/. @/    @//@   (////@ @/                                      " + "");
			Thread.sleep(20);
			System.out.println("                ////       ///      ,@////@                                     " + "");
			Thread.sleep(20);
			System.out.println("               @/////@@///////**/&  @%////&@    @@*******@                      " + "");
			Thread.sleep(20);
			System.out.println("               @@@**///////***//@@@//&@**********@//********@                   " + "");
			Thread.sleep(20);
			System.out.println("              @( @@**@///@/@/@**********************@*********@                 " + "");
			Thread.sleep(20);
			System.out.println("                    @**,*****************************@/********@                " + "");
			Thread.sleep(20);
			System.out.println("                     @/*******************/***********@*********                " + "");
			Thread.sleep(20);
			System.out.println("                       @*********/**/@**,*************@*******/*(               " + "");
			Thread.sleep(20);
			System.out.println("                         @***********/**@********************/*#                " + "");
			Thread.sleep(20);
			System.out.println("                            @/******@@    &********************@                " + "");
			Thread.sleep(20);
			System.out.println("                                           @***********/********                " + "");
			Thread.sleep(20);
			System.out.println("                                           ,********/**/********@               " + "");
			Thread.sleep(20);
			System.out.println("                                            @*****///************@              " + "");
			Thread.sleep(20);
			System.out.println("                                             ***/*****************&             " + "");
			Thread.sleep(20);
			System.out.println("                                             @*********************/@           " + "");
			Thread.sleep(20);
			System.out.println("                                              @*************,****/*/@           " + "");
			Thread.sleep(20);
			System.out.println("                                                @*************/*/@&             " + "");
			Thread.sleep(20);
			System.out.println("                                                   &@//*****/@&                 " + "");
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
	}

	public static void bolasombrasprite() {// Sprite de bola sombra
		try {
			System.out.println("                               ...,,,,,*,,,,,,...                               " + "");
			Thread.sleep(20);
			System.out.println("                         .,,*/((##%%%%&&&&%%%%##((/*,..                         " + "");
			Thread.sleep(20);
			System.out.println("                     .,*/(#%&@&&&&&&&&&&&&&&&&&&&&&&%#(/*,.                     " + "");
			Thread.sleep(20);
			System.out.println("                  .,*(#%&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&%#(*,.                  " + "");
			Thread.sleep(20);
			System.out.println("                .*/#%&&&&&&&&&&&&&%%&&&&%%&&&&&&&&&&&&&&&&&%#/*.                " + "");
			Thread.sleep(20);
			System.out.println("              .*/#&&&&&&@@@@@&&%%%%#%&&&&&&%%%%&&&&@@@@@@&&&&&#/,.              " + "");
			Thread.sleep(20);
			System.out.println("            .,/#&@&&&&&&&&&&&%##(/((((##########&&&&&&@@@@@@@@&%#/,.            " + "");
			Thread.sleep(20);
			System.out.println("           .,/#@&&&&&@@@&&%%%%####(/***///(((((###%%&%&&@@@@@@@@&#/*.           " + "");
			Thread.sleep(20);
			System.out.println("          .*(%@&&&@@@@&&&%%%##(/*//*,,...,,,*///((##%%#%&@@@@@@@@@%(*.          " + "");
			Thread.sleep(20);
			System.out.println("         .,/#&&&&@@@&&&&%%%#((//*,....      .,,***/((##%&@@@@@@@@@&#(,.         " + "");
			Thread.sleep(20);
			System.out.println("         .*#&&&&@@@@&&######/**,...           .,,*//*(/#%&&@@@@@@@@&#/.         " + "");
			Thread.sleep(20);
			System.out.println("        .,/#@&&@@@@&&%%%#(/****,.               .**/*((##&&@@@@@@@@@#/,.        " + "");
			Thread.sleep(20);
			System.out.println("        .,(%&&@@@@@&#%%###(/*,,..               .,,*/((((%&&@@@@@@@@%(*.        " + "");
			Thread.sleep(20);
			System.out.println("        .,(%&&@@@@@&%&%(##(/**,,.               .,,*///(%&&@@@@@@@@@%(,.        " + "");
			Thread.sleep(20);
			System.out.println("         ,/#@&@@@@@&&&%%%%#**/*,..             .,,**/*(%%&&@@@@@@@@&#/,.        " + "");
			Thread.sleep(20);
			System.out.println("         .*#&&@@@@@@@&%%%%#*///*,,..          .,,//((((%%&@&@@@@@@@%#*.         " + "");
			Thread.sleep(20);
			System.out.println("          ,/#&&@@@@@@&&&&%%##(((/**,....  ...,,*//(##&&&&&@@@@@@@@&#/,.         " + "");
			Thread.sleep(20);
			System.out.println("          .,/#&@@@@@@@@&&&&%%%##((///**,,,,***/(##%&&&&&&@@@@@@@@&#/,.          " + "");
			Thread.sleep(20);
			System.out.println("           .,/#&@@@@@@@@@@&&&%%%%#(####(((((##(##%%%&&&@@@@@@@@@%#/,            " + "");
			Thread.sleep(20);
			System.out.println("             .*(%&@@@@@@@@@@&&&%&&&%%%%%%%%%%###&&&@@@@@@@@@@&&%(*,             " + "");
			Thread.sleep(20);
			System.out.println("              .,*(%&@@@@@@@@@@@&&&&&&&&&&&&&%&&%%&@@@@@@@@&&&%(/,.              " + "");
			Thread.sleep(20);
			System.out.println("                .,*(#&&&@@@@@@@@@@@@@&&&&&&@@@@@@@@@@@@&&&%#(*,.                " + "");
			Thread.sleep(20);
			System.out.println("                   .,/(#%&&&&@@@@@@@@@@@@@@@@@@@@@@&&&&%#(/,.                   " + "");
			Thread.sleep(20);
			System.out.println("                      .,*/(#%%&&&&&&&&&&&&&&&&&&&&%%#(/*,.                      " + "");
			Thread.sleep(20);
			System.out.println("                          ..,**/((####%%%%####((//*,..                          " + "");
			Thread.sleep(20);
			System.out.println("                                 .....,,,,,.....                                " + "");
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
	}

	public static void bombalodosprite() { // Sprite de bomba lodo
		try {
			System.out.println("              ,,,,,,                                                            " + "");
			Thread.sleep(20);
			System.out.println("            ,,....,,,,         ,  ,                                             " + "");
			Thread.sleep(20);
			System.out.println("            ,..,,,,,,,,.      .,,,,**                                           " + "");
			Thread.sleep(20);
			System.out.println("             ,,,,,,,,,,*      *******              ,,,....                      " + "");
			Thread.sleep(20);
			System.out.println("               ,,,,,*****       *///              ******,.                      " + "");
			Thread.sleep(20);
			System.out.println("                  ********                .      .******                        " + "");
			Thread.sleep(20);
			System.out.println("     ...             *****         ,*    **       //**                          " + "");
			Thread.sleep(20);
			System.out.println("     ,***              ***,      .*****                              ,,.   ..,  " + "");
			Thread.sleep(20);
			System.out.println("                        ***      *******       *                 ***,,,,,.,....," + "");
			Thread.sleep(20);
			System.out.println("                         **     *****/////* */****           ********,,,,,,,,,,," + "");
			Thread.sleep(20);
			System.out.println("            . *           ********,,,,,,,,,,////**       *******,,,,,,,,,,,,,,  " + "");
			Thread.sleep(20);
			System.out.println("       *,    ***        .****,,   .  .,,,,,,,,,,/*******                        " + "");
			Thread.sleep(20);
			System.out.println("                 **********,,,,,,,,,,,,,,,,,,,,,,*****                          " + "");
			Thread.sleep(20);
			System.out.println("                    *****,,,,,,,,,***********////*******                        " + "");
			Thread.sleep(20);
			System.out.println("                     *********************///////********,.     /**,.,          " + "");
			Thread.sleep(20);
			System.out.println("                      ******************////////**********       ,/*            " + "");
			Thread.sleep(20);
			System.out.println("                      *****************/////**********                          " + "");
			Thread.sleep(20);
			System.out.println("                     **********************************************,            " + "");
			Thread.sleep(20);
			System.out.println("                   *****    *********************            ******,,,,,,,      " + "");
			Thread.sleep(20);
			System.out.println("         ,,,,,,,****          ********         **.             **,,,.    ...,   " + "");
			Thread.sleep(20);
			System.out.println("  .,,......,,,,,,,             *,,,,*          *****            ,,,,,,,,,,,,,,  " + "");
			Thread.sleep(20);
			System.out.println(" ,...........,,,                 ***           ****.,*           ,,,,,,,,,,,,,, " + "");
			Thread.sleep(20);
			System.out.println(" ,,,,,,,,,,,,,,                                ,...,,,**          ,,,,,,,,,,,,, " + "");
			Thread.sleep(20);
			System.out.println(" ,,,,,,,,,,,,,                    //*.          *,,***,,            ,,,,,,,,    " + "");
			Thread.sleep(20);
			System.out.println("  ,.,,,,,,,,,,                   ,*. .,           ,,,                           " + "");
			Thread.sleep(20);
			System.out.println("    ,,,,,,,,                      *,,,,                                         " + "");
			Thread.sleep(20);
			System.out.println("                                    ,.                                          " + "");
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
	}

	public static void venenoxsprite() { // Sprite de veneno X
		try {
			System.out.println("                                    @      /@                                   " + "");
			Thread.sleep(20);
			System.out.println("                               &@              ,@/                              " + "");
			Thread.sleep(20);
			System.out.println("                             @@.                 #@@                            " + "");
			Thread.sleep(20);
			System.out.println("                           (@@.                   &@@                           " + "");
			Thread.sleep(20);
			System.out.println("                          .@(@                     @.@                          " + "");
			Thread.sleep(20);
			System.out.println("                          @  @*    &@@@@@@@@@%    @@  @                         " + "");
			Thread.sleep(20);
			System.out.println("                          @   @(  (@,       #@@@ &@   &                         " + "");
			Thread.sleep(20);
			System.out.println("                         .@.   @@/             &@/   &&                         " + "");
			Thread.sleep(20);
			System.out.println("                   . %..    .   . %@@@/. .(&&%* ..        ,* ,                  " + "");
			Thread.sleep(20);
			System.out.println("                 . ,   .(%&&%@(   .. @&..,@&     .%&@%#@*    % .                " + "");
			Thread.sleep(20);
			System.out.println("                 %. *.    ,@@.   .&.&,  ,  %/ & .  @&&  .  .%. .,               " + "");
			Thread.sleep(20);
			System.out.println("                %          @@@        ,   /        @@@        (                 " + "");
			Thread.sleep(20);
			System.out.println("                           #@@*      @,    @      @@@          .&               " + "");
			Thread.sleep(20);
			System.out.println("                .            @,@     @@   ..     @*@            &               " + "");
			Thread.sleep(20);
			System.out.println("                              .@. &  @@    & ,@* @                              " + "");
			Thread.sleep(20);
			System.out.println("                                  @ @@      ,.#                                 " + "");
			Thread.sleep(20);
			System.out.println("                                  .@@        (                                  " + "");
			Thread.sleep(20);
			System.out.println("                     .@,       @@@.  (@   @     .%       .*                     " + "");
			Thread.sleep(20);
			System.out.println("                          *@@@@@@#             #&#//##                          " + "");
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
	}

	public static void placajesprite() { // Sprite de palcaje
		try {
			System.out.println("                             %%%                    #%%/                        " + "");
			Thread.sleep(20);
			System.out.println("                              %%(#%               %%%%%                         " + "");
			Thread.sleep(20);
			System.out.println("                              %%(//%%#        #%#//(%*                          " + "");
			Thread.sleep(20);
			System.out.println("                              .%%/,,/%%%(%%%(///,,(%                            " + "");
			Thread.sleep(20);
			System.out.println("                     %%%%%%%%%(#%%/,,//((///,,,,*(%                             " + "");
			Thread.sleep(20);
			System.out.println("                      ,%%//////,,,,,,,,/,,,,,,,,/%(........%%%%%                " + "");
			Thread.sleep(20);
			System.out.println("                         *#/*,,,,,,.     ,,,,,,/////(//////#%%%.                " + "");
			Thread.sleep(20);
			System.out.println("                        .%%%///,,,,,,      ,,,,,/(%%%%%%%#(/                    " + "");
			Thread.sleep(20);
			System.out.println("                   (%%(////%%%%%//,,,,,,,,,,,/(%%%                              " + "");
			Thread.sleep(20);
			System.out.println("                (%%%%%#     .%%//*/%%/,*//*,,,,//%%%%                           " + "");
			Thread.sleep(20);
			System.out.println("                            %%//%%%.%%//%%(///,,,,//%%%%(                       " + "");
			Thread.sleep(20);
			System.out.println("                           %%%%%%   %%(/%   .#%%///////#%%%                     " + "");
			Thread.sleep(20);
			System.out.println("                         ,%%%%%      %%%%        ,%%%%#//#%%%                   " + "");
			Thread.sleep(20);
			System.out.println("                         %%%(         #%*             .(#%%%%%                  " + "");
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
	}

	public static void aranazosprite() {// Sprite de aranazo
		try {
			System.out.println("                                               .%                               " + "");
			Thread.sleep(20);
			System.out.println("                                           (@&           (*                     " + "");
			Thread.sleep(20);
			System.out.println("                                       %@@@.          &@,                       " + "");
			Thread.sleep(20);
			System.out.println("                                   /@@@@/         (@@@,                         " + "");
			Thread.sleep(20);
			System.out.println("                                /@@@@@         *@@@@.                           " + "");
			Thread.sleep(20);
			System.out.println("                              /@@@@(         #@@@@*             /               " + "");
			Thread.sleep(20);
			System.out.println("                            ,@@@@*         %@@@@#            ,@                 " + "");
			Thread.sleep(20);
			System.out.println("                         .@@@@@.         &@@@@&           .&@                   " + "");
			Thread.sleep(20);
			System.out.println("                        #@@@@.        /@@@@@&           %@@                     " + "");
			Thread.sleep(20);
			System.out.println("                      ,@@@@(       .@@@@@@%          *@@@(                      " + "");
			Thread.sleep(20);
			System.out.println("                    .@@@@,       ,@@@@@@(          (@@@@.                       " + "");
			Thread.sleep(20);
			System.out.println("                   &@@@.        @@@@@@#          .@@@@%           /*            " + "");
			Thread.sleep(20);
			System.out.println("                 ,@@@#        .@@@@@@/          @@@@&          .@#              " + "");
			Thread.sleep(20);
			System.out.println("                #@@@.        *@@@@@%         .@@@@%          #@@                " + "");
			Thread.sleep(20);
			System.out.println("               %@@(         &@@@@&         (@@@@%         .@@@(                 " + "");
			Thread.sleep(20);
			System.out.println("              @@#         /@@@@@        ,@@@@@@         ,@@@@.                  " + "");
			Thread.sleep(20);
			System.out.println("            .@&         /@@@@@*        #@@@@@,         @@@@/                    " + "");
			Thread.sleep(20);
			System.out.println("            @,        ,@@@@@#        .@@@@@%         ,@@@%                      " + "");
			Thread.sleep(20);
			System.out.println("           &         (@@@@#         &@@@@&         ,@@@&                        " + "");
			Thread.sleep(20);
			System.out.println("                    *@@@(         #@@@@&         &@@@@.                         " + "");
			Thread.sleep(20);
			System.out.println("                   (@@.         %@@@@&         ,@@@@/                           " + "");
			Thread.sleep(20);
			System.out.println("                  #@,         %@@@@&         .@@@@&                             " + "");
			Thread.sleep(20);
			System.out.println("                 #(          @@@@%          &@@@*                               " + "");
			Thread.sleep(20);
			System.out.println("                %           @@@%          /@@@(                                 " + "");
			Thread.sleep(20);
			System.out.println("                           @@&           &@@@                                   " + "");
			Thread.sleep(20);
			System.out.println("                          @@.           @@@/                                    " + "");
			Thread.sleep(20);
			System.out.println("                         @#           ,@@&                                      " + "");
			Thread.sleep(20);
			System.out.println("                        @.           (@@                                        " + "");
			Thread.sleep(20);
			System.out.println("                       (             '.                                         " + "");
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
	}

	public static void golpecuerposprite() {// Sprite de golpe cuerpo
		try {
			System.out.println("                                                       (                        ");
			System.out.println("                       #      ,           ///                                   " + "");
			Thread.sleep(20);
			System.out.println("                 #%           ///*   ,  //.//  * /.                             " + "");
			Thread.sleep(20);
			System.out.println("                           //*/*.*///.////.////////,//  *,,                     " + "");
			Thread.sleep(20);
			System.out.println("                        * .////... .....*../.........../////                    " + "");
			Thread.sleep(20);
			System.out.println("                     //.///*..... .      . %( ..../....///*/.                   " + "");
			Thread.sleep(20);
			System.out.println("                      ,///.......               ...  ...*/////.,                " + "");
			Thread.sleep(20);
			System.out.println("                    ///,....                        ......*////                 " + "");
			Thread.sleep(20);
			System.out.println("                 ,...                                 ....////                  " + "");
			Thread.sleep(20);
			System.out.println("                  ////*...,#                      .....//////*                  " + "");
			Thread.sleep(20);
			System.out.println("                   /..   .....        .          . . .,/////,                   " + "");
			Thread.sleep(20);
			System.out.println("                .  *///////.    ..             .. ....*../                      " + "");
			Thread.sleep(20);
			System.out.println("                      ,///.. ...... . .  .. .*.../%/.*/ ////*                   " + "");
			Thread.sleep(20);
			System.out.println("                    ,/    .///..*,/.....,.... .....,*/.*.     *                 " + "");
			Thread.sleep(20);
			System.out.println("                    *     *//%///.....////.//. .//// .                          " + "");
			Thread.sleep(20);
			System.out.println("                           /*%///**//////,////../                               " + "");
			Thread.sleep(20);
			System.out.println("                            /,   . /  /*/ **/*/*/              .                " + "");
			Thread.sleep(20);
			System.out.println("                                   /        ..  /                               " + "");
			Thread.sleep(20);
			System.out.println("                           %    *            /                                  " + "");
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
	}

	public static void golpeosprite() { // Sprite de golpeo
		try {
			System.out.println("     ..                       .                               .                 " + "");
			Thread.sleep(20);
			System.out.println("       .....                  ...             ....          ..                  " + "");
			Thread.sleep(20);
			System.out.println("         //*.....            ./*...        ...//...       /,                    " + "");
			Thread.sleep(20);
			System.out.println("   .      .//////.....       ,////...    ..//////...   ,//       .......        " + "");
			Thread.sleep(20);
			System.out.println("     .*     ,/////////,....  ///////,...//////////...///......,*////,           " + "");
			Thread.sleep(20);
			System.out.println("       .//.   //(#(////////*.///%//////////(%///////(#//////////                " + "");
			Thread.sleep(20);
			System.out.println("         .///* .//#%%%%%(//////(%%%#////#%%%%%///(%#////////,/............      " + "");
			Thread.sleep(20);
			System.out.println("           .///(#,//#%%%%%%%%#(%%%%%%%%%%%%%%%%%%%%%%%%%#(///////////////....   " + "");
			Thread.sleep(20);
			System.out.println("          .../////%%#/#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%###((/////////////,....    " + "");
			Thread.sleep(20);
			System.out.println("        ...////////#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%(///////....      " + "");
			Thread.sleep(20);
			System.out.println("      ...////*.*/#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#///////.....       " + "");
			Thread.sleep(20);
			System.out.println("    .. /.   ..*#(///(%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%///////*....         " + "");
			Thread.sleep(20);
			System.out.println("           ..//////#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%(///////....        " + "");
			Thread.sleep(20);
			System.out.println("          ..//////%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%////////....      " + "");
			Thread.sleep(20);
			System.out.println("         ..//////%%#.///(%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%(((###%%%///////*...     " + "");
			Thread.sleep(20);
			System.out.println("        ../////... .////%%#/(%%%%%%%%%%%%%%%%%%%%(%%%%%#/////////////////....   " + "");
			Thread.sleep(20);
			System.out.println("       ..,...      *///#/////(%%%#///(%%%%%%%%%%%///#%%%//////................  " + "");
			Thread.sleep(20);
			System.out.println("      ..          .///* ..////#%%*///////(%%%%%%%////,.%#/////*...              " + "");
			Thread.sleep(20);
			System.out.println("                 .//,.   .,////%(....////////(%%#////... */////...              " + "");
			Thread.sleep(20);
			System.out.println("                 ,*.     ..*////.   .....////////////...   .///...              " + "");
			Thread.sleep(20);
			System.out.println("                ..        ..///*       .....*////////...     .,*...             " + "");
			Thread.sleep(20);
			System.out.println("                           ..//.          ....../////...        ...             " + "");
			Thread.sleep(20);
			System.out.println("                            .,,               .....,/...          .             " + "");
			Thread.sleep(20);
			System.out.println("                            ...                  ......                         " + "");
			Thread.sleep(20);
			System.out.println("                             .                      ...                         " + "");
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
	}

	public static void ataquealasprite() {// Sprite de ataque ala
		try {
			System.out.println("                                  @@@@@.@                                       " + "");
			Thread.sleep(20);
			System.out.println("                                 @@@@@@,@@                                      " + "");
			Thread.sleep(20);
			System.out.println("                                @(#@@@@@@@@                                     " + "");
			Thread.sleep(20);
			System.out.println("                                 @@%(@&@@@@@/                                   " + "");
			Thread.sleep(20);
			System.out.println("                                 #%@@&@@@@*@@ ,                                 " + "");
			Thread.sleep(20);
			System.out.println("                                  @@/@@&@&@%*/&*                                " + "");
			Thread.sleep(20);
			System.out.println("                                  #@/@#@@@&@%@#                                 " + "");
			Thread.sleep(20);
			System.out.println("                                 ..@@@@/&&@%@@                                  " + "");
			Thread.sleep(20);
			System.out.println("                                 .@.@@&@@#@/&%.                                 " + "");
			Thread.sleep(20);
			System.out.println("                                  @@@@@(@@&@##@.                                " + "");
			Thread.sleep(20);
			System.out.println("                                 ,@@%#@(@@#&%#@,                                " + "");
			Thread.sleep(20);
			System.out.println("                                (.@%&*&@/@#&@@%                                 " + "");
			Thread.sleep(20);
			System.out.println("                                 &#%*@//@#(@@(@/                                " + "");
			Thread.sleep(20);
			System.out.println("                                . @@(,&(&#*@@%&.                                " + "");
			Thread.sleep(20);
			System.out.println("                                  @&(%#(#@(@&#@(                                " + "");
			Thread.sleep(20);
			System.out.println("                                   @@@@@&&%@#&#                                 " + "");
			Thread.sleep(20);
			System.out.println("                                    .@&&&&&%& (,                                " + "");
			Thread.sleep(20);
			System.out.println("                                    %@@@@(@%#                                   " + "");
			Thread.sleep(20);
			System.out.println("                                    ,/./&&  *,                                  " + "");
			Thread.sleep(20);
			System.out.println("                                        #.  ,                                   " + "");
			Thread.sleep(20);
			System.out.println("                                       /.                                       " + "");
			Thread.sleep(20);
			System.out.println("                                       #                                        " + "");
			Thread.sleep(20);
			System.out.println("                                      @.                                        " + "");
			Thread.sleep(20);
			System.out.println("                                      %                                         " + "");
			Thread.sleep(20);
			System.out.println("                                     @                                          " + "");
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
	}

	public static void vuelosprite() {// Sprite de vuelo
		try {
			System.out.println("                                                      (&,                       " + "");
			Thread.sleep(20);
			System.out.println("                     @%%@&&%&                       (#@&&%%#                    " + "");
			Thread.sleep(20);
			System.out.println("                   &@&&%%%%&@                      &%(#/%/%&%@                  " + "");
			Thread.sleep(20);
			System.out.println("                 &@@#&#&&@&#&&                    #/%%%##%&/@&&&                " + "");
			Thread.sleep(20);
			System.out.println("                &&&&%%(&%(@%%@%@#               %&#&#&%(&*&@%%%%&,              " + "");
			Thread.sleep(20);
			System.out.println("               @&%#&&%%&#@&%&/%%@%(@%      .%(#@#(&#&%&@@&&&&@&&&&*             " + "");
			Thread.sleep(20);
			System.out.println("              %&&&&#@(@@&%%%%@%#%@%%#/&&@%/&(/#&(@&%%(#*@%&&@&(%#@%%            " + "");
			Thread.sleep(20);
			System.out.println("             &&#&@%&&&(%&%%&@(&#&##&%&%%%&(##&%%%&%(@&&%&#&@#%%&&&@&.           " + "");
			Thread.sleep(20);
			System.out.println("            (@@&%%#&%#%##&@@&&%%&%&&&&%&&#%&%%%(%%#%&%&&(%#%&@#@#%#&&           " + "");
			Thread.sleep(20);
			System.out.println("           @&&&&@(&%%&#%&@#&##@@#%@%&&#&&%@&%&%(&%##%&%%%(&#%&(%&(@&&&.         " + "");
			Thread.sleep(20);
			System.out.println("           @&%%(&(&(%%(%%((#&%&%(&%#&#&#/*%%##(/#(##(&&%/%,@%%(%/%/%#&#         " + "");
			Thread.sleep(20);
			System.out.println("          %@%%(%(#(#&#%%%/,%/ /((%#%#%#/ #(/#*#%&%%   (&*%%&(%#%(&#(%%&&        " + "");
			Thread.sleep(20);
			System.out.println("        %%@&%@#%(&#%&%/%%(%      &#&##@#,@/#&%%%(      %.%%(%%#@@#&#&%&(&.      " + "");
			Thread.sleep(20);
			System.out.println("         #@%#(%&%%&&(%(&#           @ @  @ @            .%&/%&#%*&%&@#/&%       " + "");
			Thread.sleep(20);
			System.out.println("        #&/(%(/&(%(%# #                                   &#%(&#%#&#(&##@#      " + "");
			Thread.sleep(20);
			System.out.println("      %%&(#&(#%#&%#&% %                                     (%&#&(#%#(&%#/@.    " + "");
			Thread.sleep(20);
			System.out.println("     &@##%&(#%(#&(%,&                                        ( &(&%(&#(@&#%     " + "");
			Thread.sleep(20);
			System.out.println("      #%&((%%/#&%%                                             *& &%/&&#%&&%    " + "");
			Thread.sleep(20);
			System.out.println("    .&&.(#& (&* %                                                  &&% &&% %%   " + "");
			Thread.sleep(20);
			System.out.println("   &# &%%  #%                                                       &#  &%&     " + "");
			Thread.sleep(20);
			System.out.println("    &&&   #*                                                               @#   " + "");
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
	}

	public static void alientodragonsprite() {// Sprite de aliento dragon
		try {
			System.out.println("  .@@@@@@@@@@@                                                                  " + "");
			Thread.sleep(20);
			System.out.println("  .@@@@@@@@@@@@@(                                                               " + "");
			Thread.sleep(20);
			System.out.println("  .@@@@@@@@@@@@@@@@                                                             " + "");
			Thread.sleep(20);
			System.out.println("    &@@@@@@@@@@@@@@@@#                                                          " + "");
			Thread.sleep(20);
			System.out.println("       #@@@@@@@@@@@@@@@@.                                        @@@@@@@#       " + "");
			Thread.sleep(20);
			System.out.println("          &@@@@@@@@@@@@@@@@                                    @@@@@@@@@@@@.    " + "");
			Thread.sleep(20);
			System.out.println("             @@@@@@@@@@@@@@@@/                                @#    #@@@@@@@@   " + "");
			Thread.sleep(20);
			System.out.println("               &@@@@@@@@@@@@@@@@,                                       @@@@@&  " + "");
			Thread.sleep(20);
			System.out.println("                 &@@@@@@@@@@@@@@@@@,          *,                 (@@@@@@@@#@@@  " + "");
			Thread.sleep(20);
			System.out.println("                  .@@@@@@@@@@@@@@@@@@@(        @@.             %@&/,*&@@@@@@@@  " + "");
			Thread.sleep(20);
			System.out.println("                   %@@@@@@@@@@@@@@@@@@@@@@#   %@@@       /              @@@@@@  " + "");
			Thread.sleep(20);
			System.out.println("              /@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@     @@               @@@@@  " + "");
			Thread.sleep(20);
			System.out.println("                  %@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@(   ,@@@              *@@@@  " + "");
			Thread.sleep(20);
			System.out.println("                       (@@@@@@@@@@@@@@@@@@@@@@@@@@@@    @@@@             *@@@@  " + "");
			Thread.sleep(20);
			System.out.println("                            (@@@@@@@@@@@@@@@@@@@@@@@@   #@@@@@           @@@@@  " + "");
			Thread.sleep(20);
			System.out.println("                                @@@@@@@@@@@@@@@@@@@@@@*  *@@@@@@&      .@@@@@@  " + "");
			Thread.sleep(20);
			System.out.println("                                  /@@@@@@@@@@@@@@@@@@@@@/  %@@@@@@@@@@@@@@@@@@  " + "");
			Thread.sleep(20);
			System.out.println("                                    ,@@@@@@@@@@@@@@@@@@@@@@@/ (@@@@@@@@@@@@@@@  " + "");
			Thread.sleep(20);
			System.out.println("                                      /@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  " + "");
			Thread.sleep(20);
			System.out.println("                                        &@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  " + "");
			Thread.sleep(20);
			System.out.println("                                         /@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  " + "");
			Thread.sleep(20);
			System.out.println("                                          @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  " + "");
			Thread.sleep(20);
			System.out.println("                   *@@@*                  @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  " + "");
			Thread.sleep(20);
			System.out.println("            *(    @@@@@.                 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  " + "");
			Thread.sleep(20);
			System.out.println("           /@@    @@@@@@@&            *@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  " + "");
			Thread.sleep(20);
			System.out.println("           @@@@&  (@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  " + "");
			Thread.sleep(20);
			System.out.println("           .@@@@@@@,@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  " + "");
			Thread.sleep(20);
			System.out.println("             @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  " + "");
			Thread.sleep(20);
			System.out.println("                &@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  " + "");
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
	}

	public static void garradragonsprite() {// Sprite de garra dragon
		try {
			System.out.println("                              *@@@@@@@@@                                        " + "");
			Thread.sleep(20);
			System.out.println("           #@@@@@@@@@@@.         &@@@@@@@@@                                     " + "");
			Thread.sleep(20);
			System.out.println("              ,@@@@@@@@@@@@@       ,@@@@@@@@#                                   " + "");
			Thread.sleep(20);
			System.out.println("                   ,@@@@@@@@@@@.     .@@@@@@@@.                                 " + "");
			Thread.sleep(20);
			System.out.println("               *@@.    .@@@@@@@@@@*           ,#                                " + "");
			Thread.sleep(20);
			System.out.println("          @@@@@@@@@@@@.    @@@@@@@@@   @@@@%/.                                  " + "");
			Thread.sleep(20);
			System.out.println("       &@@@@@@@@@@@@@@@@@*   ,@@@,   ,@@@@@@@@@@@@@@@/                          " + "");
			Thread.sleep(20);
			System.out.println("     &@@@@@@@@@@@@@@@@@@@@@@      ,@@@@@@@@@@@@@@@@@@@@@@(                      " + "");
			Thread.sleep(20);
			System.out.println("    @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@%*...,#@@@@@@@@@@@@@@#                    " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@&       .@@@@@@@@@@@@(                   " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@&      &@@@@@@@@@@@@@                  " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@@@@@@@@@@@@@@%   @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@/               " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@@@@@@@@@@@@@@   @@@@@@@@@@@@@@@%#@@@@@@@@@@@@@@@@@@(           " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@@@@@.          @@@@@@@@@@@@&    &@@@@@@@@@@@@@@@@@@@@@@@@@     " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@@@            @@@@@@@@@@@@(     @@@/    @@@@@@@@@@@@@@@@@@@.   " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@@              @@@@@@@@@@@@      .@@@%  .@@@@@@@@@@@@@@@@@@@   " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@@               #@@@@@@@@@@@.     @@@         @@@@@@@@@@@@@@   " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@#                 @@@@@@@@@@      .@@@@@&##.   (      /@@@@@   " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@@                  *@@@@@@@@@(       @@@.               %@@,   " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@@                    #@@@@@@@@@@@*   @@@.                @.    " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@@%                     /@@@@@@@@@/    *@@@@@@@%                " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@@@(                       @@@@@@@@#       /@@*                 " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@@@@/                        ,@@@@@@@@@@@@  &@@@&               " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@@@@@&                           @@@@@@@@@@  ,@@@@.             " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@@@@@@@                              *@@@@@@@@@.                " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@@@@@@@@@                                                       " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@@@@@@@@@@@                                                     " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@@@@@@@@@@@@@&                                                  " + "");
			Thread.sleep(20);
			System.out.println("   ,@@@@@@@@@@@@@@@@@@@@@@@@@@@@@/                                              " + "");
			Thread.sleep(20);
		} catch (InterruptedException e) {
		}
	}

	public static void charizardsprite() {// Sprite de charizard
		System.out.println("                .\"-,.__\r\n" + "                 `.     `.  ,\r\n"
				+ "              .--'  .._,'\"-' `.\r\n" + "             .    .'         `'\r\n"
				+ "             `.   /          ,'\r\n" + "               `  '--.   ,-\"'\r\n"
				+ "                `\"`   |  \\\r\n" + "                   -. \\, |\r\n"
				+ "                    `--Y.'      ___.\r\n" + "                         \\     L._, \\\r\n"
				+ "               _.,        `.   <  <\\                _\r\n"
				+ "             ,' '           `, `.   | \\            ( `\r\n"
				+ "          ../, `.            `  |    .\\`.           \\ \\_\r\n"
				+ "         ,' ,..  .           _.,'    ||\\l            )  '\".\r\n"
				+ "        , ,'   \\           ,'.-.`-._,'  |           .  _._`.\r\n"
				+ "      ,' /      \\ \\        `' ' `--/   | \\          / /   ..\\\r\n"
				+ "    .'  /        \\ .         |\\__ - _ ,'` `        / /     `.`.\r\n"
				+ "    |  '          ..         `-...-\"  |  `-'      / /        . `.\r\n"
				+ "    | /           |L__           |    |          / /          `. `.\r\n"
				+ "   , /            .   .          |    |         / /             ` `\r\n"
				+ "  / /          ,. ,`._ `-_       |    |  _   ,-' /               ` \\\r\n"
				+ " / .           \\\"`_/. `-_ \\_,.  ,'    +-' `-'  _,        ..,-.    \\`.\r\n"
				+ ".  '         .-f    ,'   `    '.       \\__.---'     _   .'   '     \\ \\\r\n"
				+ "' /          `.'    l     .' /          \\..      ,_|/   `.  ,'`     L`\r\n"
				+ "|'      _.-\"\"` `.    \\ _,'  `            \\ `.___`.'\"`-.  , |   |    | \\\r\n"
				+ "||    ,'      `. `.   '       _,...._        `  |    `/ '  |   '     .|\r\n"
				+ "||  ,'          `. ;.,.---' ,'       `.   `.. `-'  .-' /_ .'    ;_   ||\r\n"
				+ "|| '              V      / /           `   | `   ,'   ,' '.    !  `. ||\r\n"
				+ "||/            _,-------7 '              . |  `-'    l         /    `||\r\n"
				+ ". |          ,' .-   ,' ||               | .-.        `.      .'     ||\r\n"
				+ " `'        ,'    `\".'    |               |    `.        '. -.'       `'\r\n"
				+ "          /      ,'      |               |,'    \\-.._,.'/'\r\n"
				+ "          .     /        .               .       \\    .''\r\n"
				+ "        .`.    |         `.             /         :_,'.'\r\n"
				+ "          \\ `...\\   _     ,'-.        .'         /_.-'\r\n"
				+ "           `-.__ `,  `'   .  _.>----''.  _  __  /\r\n"
				+ "                .'        /\"'          |  \"'   '_\r\n"
				+ "               /_|.-'\\ ,\".             '.'`__'-( \\\r\n"
				+ "                 / ,\"'\"\\,'               `/  `-.|\"");
	}

	public static void charizardespaldasprite() {// Sprite de charizard de espaldas
		System.out.println("                                                    .                           ");
		System.out.println("                                               ***,          /*                 " + "");
		System.out.println("                                           ******             //*.              " + "");
		System.out.println("                                         *****                  //*,,*          " + "");
		System.out.println("                                     ,*****,              ***///////*,*,        " + "");
		System.out.println("                                  ******,,*     *******//////*  /*,//**,*,      " + "");
		System.out.println("                                *******,,&/*********(%&&&&//,     (//***,,,,,   " + "");
		System.out.println("                              ********,%&&&&(****&&&&&&&(/******/*////*,#/*,,   " + "");
		System.out.println("                             ********,&&&&&&&&#/**/&&&(//*****/(((***(/*******,," + "");
		System.out.println("                           *********%&&&&&&&&&&&&&(##%(********///  ********,/**" + "");
		System.out.println("                           (*****,     (&&&&&&&&&&&%*********,,                 " + "");
		System.out.println("                           ****,           .//****,,,,***/*********             " + "");
		System.out.println("                          ****                 ////////((//**** ///. .          " + "");
		System.out.println("                         ***            /**   //////*******,                    " + "");
		System.out.println("                        ,*,              /*,,**/////*******,                    " + "");
		System.out.println("                       **.              ///*,***////*****/******                " + "");
		System.out.println("                      **             //////***********,,,(*********             " + "");
		System.out.println("          (((                      **/////********,,,,,,,*/*******,,,           " + "");
		System.out.println("        ,,,((                    ,*//////******,,,,,,,,,,.,////*****,,,         " + "");
		System.out.println("    /((*,(###                    *////******,,,,,,,,,,....,*((////***,,,        " + "");
		System.out.println("    (((,,(/                       /*****,,,,,,,,,,,.....,,  .((/////***         " + "");
		System.out.println("*((#,,,*##*(                     *****,,,,,,,,,,.....,     .(///***             " + "");
		System.out.println(" ((,,,,((((*                  *****,,,,,,,,,.....,          *(//**,,*           " + "");
		System.out.println("  ***(((((               ,**********,,,,*.....                (///*,,,..        " + "");
		System.out.println("  ****#.             ***************,.....                      ///*, ,..       " + "");
		System.out.println("  *****        *****************,....,                            (//*.         " + "");
		System.out.println("   /////////****//////////,,,,,,.                                               " + "");
		System.out.println("       (((((///////****                                                         " + "");
	}

	public static void blastoisesprite() { // Sprite de blastoise
		System.out.println("                    _\r\n" + "            _,..-\"\"\"--' `,.-\".\r\n"
				+ "          ,'      __.. --',  |\r\n" + "        _/   _.-\"' |    .' | |       ____\r\n"
				+ "  ,.-\"\"'    `-\"+.._|     `.' | `-..,',--.`.\r\n"
				+ " |   ,.                      '    j 7    l \\__\r\n"
				+ " |.-'                            /| |    j||  .\r\n"
				+ " `.                   |         / L`.`\"\"','|\\  \\\r\n"
				+ "   `.,----..._       ,'`\"'-.  ,'   \\ `\"\"'  | |  l\r\n"
				+ "     Y        `-----'       v'    ,'`,.__..' |   .\r\n"
				+ "      `.                   /     /   /     `.|   |\r\n"
				+ "        `.                /     l   j       ,^.  |L\r\n"
				+ "          `._            L       +. |._   .' \\|  | \\\r\n"
				+ "            .`--...__,..-'\"\"'-._  l L  \"\"\"    |  |  \\\r\n"
				+ "          .'  ,`-......L_       \\  \\ \\     _.'  ,'.  l\r\n"
				+ "       ,-\"`. / ,-.---.'  `.      \\  L..--\"'  _.-^.|   l\r\n"
				+ " .-\"\".'\"`.  Y  `._'   '    `.     | | _,.--'\"     |   |\r\n"
				+ "  `._'   |  |,-'|      l     `.   | |\"..          |   l\r\n"
				+ "  ,'.    |  |`._'      |      `.  | |_,...---\"\"\"\"\"`    L\r\n"
				+ " /   |   j _|-' `.     L       | j ,|              |   |\r\n"
				+ "`--,\"._,-+' /`---^..../._____,.L',' `.             |\\  |\r\n"
				+ "   |,'      L                   |     `-.          | \\j\r\n"
				+ "            .                    \\       `,        |  |\r\n"
				+ "             \\                __`.Y._      -.     j   |\r\n"
				+ "              \\           _.,'       `._     \\    |  j\r\n"
				+ "              ,-\"`-----\"\"\"\"'           |`.    \\  7   |\r\n"
				+ "             /  `.        '            |  \\    \\ /   |\r\n"
				+ "            |     `      /             |   \\    Y    |\r\n"
				+ "            |      \\    .             ,'    |   L_.-')\r\n"
				+ "             L      `.  |            /      ]     _.-^._\r\n"
				+ "              \\   ,'  `-7         ,-'      / |  ,'      `-._\r\n"
				+ "             _,`._       `.   _,-'        ,',^.-            `.\r\n"
				+ "          ,-'     v....  _.`\"',          _:'--....._______,.-'\r\n"
				+ "        ._______./     /',,-'\"'`'--.  ,-'  `.\r\n"
				+ "                 \"\"\"\"\"`.,'         _\\`----...\r\n" + "                        --------\"\"'");

	}

	public static void blastoiseespaldasprite() {// Sprite de blastoise de espaldas
		System.out.println("                                           ,*/(/////////////*                   " + "");
		System.out.println("                                          /(&&(////##..,/%((//////*             " + "");
		System.out.println("                                          ,/(#///////////////((%%#(.            " + "");
		System.out.println("                                           /(((((((#(////////(,,,,.             " + "");
		System.out.println("                                          #((((((////////,,,,,,,,*              " + "");
		System.out.println("               .**,.                .*(#####((*....*//////,,,,,,*               " + "");
		System.out.println("             ,/,,,,,..        (((((((((((((((((((%##%..*//////,                 " + "");
		System.out.println("              //,,,,,.(((((###%####%##((((#%(((((((%%%%,..,      ,/,.../,*      " + "");
		System.out.println("               .//,/(%%%#(((((((((((((((#(((((((((%%%&&%%...  ,,,,,,,,,*&&/     " + "");
		System.out.println("                 *%%%%(((((((((((((((((#(((((((((#%%/,,,,,,,,/**///////(#&*     " + "");
		System.out.println("                %%%#(((((((((((((((((((%(((((((((%%%**/,,,*//##/////////(,      " + "");
		System.out.println("               %%%(((((((((((((((((((((#(((((((((%%#///#/(..#///..,(,           " + "");
		System.out.println("             /(((((((((((((((((((((((((%##%%%%%%%%%%&&,,...(//////*(..(         " + "");
		System.out.println("           ,(/((((((((((((((((((((((#%%%%%%%%%%%%%%%%&,,,,,#((////(//,.//.      " + "");
		System.out.println("            ,#(((((((((((((((((((#%%%%%%%%%%%%%&&%%%%&*,,,,#(((((#//////*./     " + "");
		System.out.println("             #(((((((((((((((#%%%%%%%%%%%%%%%%%%%%%&&&%*,,,#((#((((((//#//,(    " + "");
		System.out.println("            ,%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#,*,,,#(((((((((#(((((/..  " + "");
		System.out.println("           .%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%,..,#,,///,    ,//#/  **    " + "");
		System.out.println("           /%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#....,(((///                  " + "");
		System.out.println("          ((%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%#......(///#/                   " + "");
		System.out.println("         ///%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%.....,,,*////,                    " + "");
		System.out.println("        (///%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%/....,,,*#(((/...,                   " + "");
		System.out.println("       .////%%%%%%%%%%%%%%%%%%%%%%%%%%%%%....,,,,#((((((//*....                 " + "");
		System.out.println("       (////.#%%%%%%%%%%%%%%%%%%%%%%%%/....,,,,(((((((((#////..                 " + "");
		System.out.println("       #((((*...,,,,,...*%%%%%%%%/......,,,,,#(((((((((((//////(                " + "");
		System.out.println("        (((((*,/(/////#*,,.....,,,,,,,,,,,,#(((((((((((((#//////                " + "");
		System.out.println("        (###((/////////((((,,,,,/(//////((((((((((((((((((#(((#                 " + "");
		System.out.println("       /***(//////////(((((#/////////(*.  (((((((((((((((((#((                  " + "");
		System.out.println("          /////////(((((((                .((((((((((((((((#                    " + "");
		System.out.println("        (///////(((((((/                   ##*/#(((((((((((/*...                " + "");
		System.out.println("       ((((((((((#/.                      /******/(#(/**,  *///.                " + "");
	}

	public static void venusaursprite() { // Sprite de venusaur
		System.out.println("  					_._       _,._\r\n" + "                        _.'   `. ' .'   _`.\r\n"
				+ "                ,\"\"\"/`\"\"-.-.,/. ` V'\\-,`.,--/\"\"\".\"-..\r\n"
				+ "              ,'    `...,' . ,\\-----._|     `.   /   \\\r\n"
				+ "             `.            .`  -'`\"\" .._   :> `-'   `.\r\n"
				+ "            ,'  ,-.  _,.-'| `..___ ,'   |'-..__   .._ L\r\n"
				+ "           .    \\_ -'   `-'     ..      `.-' `.`-.'_ .|\r\n"
				+ "           |   ,',-,--..  ,--../  `.  .-.    , `-.  ``.\r\n"
				+ "           `.,' ,  |   |  `.  /'/,,.\\/  |    \\|   |\r\n"
				+ "                `  `---'    `j   .   \\  .     '   j\r\n"
				+ "              ,__`\"        ,'|`'\\_/`.'\\'        |\\-'-, _,.\r\n"
				+ "       .--...`-. `-`. /    '- ..      _,    /\\ ,' .--\"'  ,'\".\r\n"
				+ "     _'-\"\"-    --  _`'-.../ __ '.'`-^,_`-\"\"\"\"---....__  ' _,-`\r\n"
				+ "   _.----`  _..--.'        |  \"`-..-\" __|'\"'         .\"\"-. \"\"'--.._\r\n"
				+ "  /        '    /     ,  _.+-.'  ||._'   \"\"\"\". .          `     .__\\\r\n"
				+ " `---    /        /  / j'       _/|..`  -. `-`\\ \\   \\  \\   `.  \\ `-..\r\n"
				+ ",\" _.-' /    /` ./  /`_|_,-\"   ','|       `. | -'`._,   L  \\ .  `.   |\r\n"
				+ "`\"' /  /  / ,__...-----| _.,  ,'            `|----.._`-.|' |. .` ..  .\r\n"
				+ "   /  '| /.,/   \\--.._ `-,' ,          .  '`.'  __,., '  ''``._ \\ \\`,'\r\n"
				+ "  /_,'---  ,     \\`._,-` \\ //  / . \\    `._,  -`,  / / _   |   `-L -\r\n"
				+ "   /       `.     ,  ..._ ' `_/ '| |\\ `._'       '-.'   `.,'     |\r\n"
				+ "  '         /    /  ..   `.  `./ | ; `.'    ,\"\" ,.  `.    \\      |\r\n"
				+ "   `.     ,'   ,'   | |\\  |       \"        |  ,'\\ |   \\    `    ,L\r\n"
				+ "   /|`.  /    '     | `-| '                  /`-' |    L    `._/  \\\r\n"
				+ "  / | .`|    |  .   `._.'                   `.__,'   .  |     |  (`\r\n"
				+ " '-\"\"-'_|    `. `.__,._____     .    _,        ____ ,-  j     \".-'\"'\r\n"
				+ "        \\      `-.  \\/.    `\"--.._    _,.---'\"\"\\/  \"_,.'     /-'\r\n"
				+ "         )        `-._ '-.        `--\"      _.-'.-\"\"        `.\r\n"
				+ "        ./            `,. `\".._________...\"\"_.-\"`.          _j\r\n"
				+ "       /_\\.__,\"\".   ,.'  \"`-...________.---\"     .\".   ,.  / \\\r\n"
				+ "              \\_/\"\"\"-'                           `-'--(_,`\"`-`");
	}

	public static void venusaurespladasprite() {// Sprite de venusaur de espaldas
		System.out.println("                                       ,#%                                      " + "");
		System.out.println("                                     .%**/*#                                    " + "");
		System.out.println("                                 ***(///////%****                               " + "");
		System.out.println("                         ,%*((%%/,///////#((%%******%##&%                       " + "");
		System.out.println("                      *##/#/#(////**/(/(#(((/(/%/.*/*/*/(%#%*                   " + "");
		System.out.println("                    &(. %##%(#/((((((((%(((((####%////////#%%#                  " + "");
		System.out.println("                                   .%(/((((/(#                                  " + "");
		System.out.println("                           ..&%(#((#%%%(///(%%(#%#((/(%%,                       " + "");
		System.out.println("                      ##(((((((((((%(%%%%(/#(////////#//((%%%#                  " + "");
		System.out.println("                  .%(((((((((#(((//*****,,,,,*(//(////////////#/                " + "");
		System.out.println("                 #(((((((((%%(//////***********//%%#(/////////##                " + "");
		System.out.println("                #%/(%(%///////////***,,*/(#(//////////**((#//#%%                " + "");
		System.out.println("              %/((/(/////#///////////#*****,,,(#///////*,(//**&                 " + "");
		System.out.println("             %///////////////////////////((/**,*(/#(/////#//&&*                 " + "");
		System.out.println("             &/////////(/////(((///////////////**///////(##/(                   " + "");
		System.out.println("              /#/////////////////////////#/////*(((///////(                     " + "");
		System.out.println("              ,(//////%%((//////////#%////(////( %(////////(                    " + "");
		System.out.println("              ,,,,,,,,,,           #/////////(%  &/////////((                   " + "");
		System.out.println("                                   #((((((((((,                                 " + "");

	}

	public static void gengarsprite() {// Sprite de gengar
		System.out.println("            %&&&(                               &                               " + "");
		System.out.println("             &&&&&&&&                       &&&&@                               " + "");
		System.out.println("              &&&&&&&&&&&     &%   @    &&&&&&&&                  @&@@&&&&&     " + "");
		System.out.println("              *&&&&&&&&&&&&&&/&&&&&&&&&&&&&&&&&&@&&&    @@@&@&&&&&&&&@@&&       " + "");
		System.out.println("               @@&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&@@&&&&&&&&&&&&         " + "");
		System.out.println("                &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&           " + "");
		System.out.println("                 &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&             " + "");
		System.out.println("     &%&&&%&&     &&&&&&%&%%&%%&%&&&&&&&&&&&&&&&&&&&&&&&&&&&%%%&%.              " + "");
		System.out.println("   *%%%%%#%&%%%%%%&%%%%(%%%%%%%%%&%&&&&&&&&%&&&%&&&&&&&&&&%##&&#                " + "");
		System.out.println("    *###((##&%(####(######%%%%%&&%%&&&%%%%&&&&&%%&%%&&&&&%#(%&                  " + "");
		System.out.println("        ((#(&&/#((/***//// *###%%%%%%&&&&%&##%%&&&(&&&&%%(%&%%&&%%&&&&&&&%%&%%  " + "");
		System.out.println("          *#&&&&&&&&&&**    (((#%%%(#%%###%%%%&   ,&%%%###%%%%%%#%%%&%%%%&&     " + "");
		System.out.println("               (%&&&&&& ,     (((##%#####%##%#     #%%%%%%%%%%%%%%%%%&&&%&,/    " + "");
		System.out.println("              &&&&&&&&&&&/     /###%%%&%%#       &&&&&%%%%%%%#%#####%%          " + "");
		System.out.println("              (//,. .. **((######%#%#%#%%%%%%%%%%#%%###%%%&&%#%%#/(             " + "");
		System.out.println("              &&&&&&*/   ####%#%%%%##%###%%%%%%&%% %%%%##%%%%#.                 " + "");
		System.out.println("              **/*/((##/  #     #  .*/##%%#.    ,  %%&&&%%%(%%                  " + "");
		System.out.println("              .///((##%/(*/     #        &      ,(&&&&%%#%&%&                   " + "");
		System.out.println("               ((####(##%%##%(  #        &    ,&&&&&&&&&&&&&/                   " + "");
		System.out.println("                */((((#%%%&&&&&@&&&&&&&&&&&&&&&&&&&&&&&&&&&&                    " + "");
		System.out.println("               *,,&&&&&&#%&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&@                    " + "");
		System.out.println("               (&&&&&&&&/###%&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&                   " + "");
		System.out.println("               ((#&&&&&&(#%%%#%&&&#&&&&&&&&&&&&&&&&&&&&&&&&&@                   " + "");
		System.out.println("                %&&&&&&##%%%%%%%&&&&&&&&&&&&&&&@&&&&&&&&&&&&&                   " + "");
		System.out.println("                   ##%%%%%&&& &&&&&&&&&&&&&&&&&&&&&&&@&&&&&&&                   " + "");
		System.out.println("                       &&&&&         &&&&&&&    &&&&&&&&&&&/                    " + "");
		System.out.println("                                         *#       &&&&&&&(                      " + "");
	}

	public static void gengarespladasprite() { // Sprite de gengar de espaldas
		System.out.println("                                                                           /@@  ");
		System.out.println("           @@@@                            &,                         @@@&##%@  " + "");
		System.out.println("           @@####@@@                       @%@@@                @@@@########@&  " + "");
		System.out.println("            @@########@@@    @#%@@@        @%###%@@        @@@#############@@   " + "");
		System.out.println("             @#############@@@#######@@@@  @%######%@@.@@&################@@    " + "");
		System.out.println("              @############################@#############################@@     " + "");
		System.out.println("               @########################################################@@      " + "");
		System.out.println("                @######################################################@@       " + "");
		System.out.println("                ,@&###################################################@@        " + "");
		System.out.println("             @@@#####################################################@@         " + "");
		System.out.println("          @@#########################################################@@         " + "");
		System.out.println("         @@############################################################@@       " + "");
		System.out.println("            @@&########################################################@/@&     " + "");
		System.out.println("           @@@#&@@####################################################@&//#@    " + "");
		System.out.println("      #@@&#########@@###############@#################################@(///(@   " + "");
		System.out.println("     @@###############@#########&@@##############################%####@@///@@@  " + "");
		System.out.println("        @@@##################@@@################################@.@@###@@///@@@ " + "");
		System.out.println("          @@@@@&##########&@@###################################@   @@###%@@@@@@" + "");
		System.out.println("       @@&#######@@#########&@@@################################@   @/@@#######@" + "");
		System.out.println("     @@###########################@@@@##########################@(  @   @@@@@&@@" + "");
		System.out.println("   @@@@@@@@@@@@@&################################################@  @   @    @  " + "");
		System.out.println("       @@##################&@@####################################@@@   @    @  " + "");
		System.out.println("     @@#################@@&##########################################@@ @    @  " + "");
		System.out.println("  @@#########@#######@@%##########################################@@@&##@@%  @  " + "");
		System.out.println("@@#######@@@#######@@#################################################&@@##&@@  " + "");
		System.out.println(" %@@@@&@################%%%%%%%%%%%######################################@@###@@" + "");

	}

	public static void snorlaxsprite() {// Sprite de snorlax
		System.out.println("                       %%%%%&%               @%%%%%%&&                          " + "");
		System.out.println("                       %%%%%%%%%%%%%%%%%%%#&&%%%%%%%&&                          " + "");
		System.out.println("                       %%%%..... ..... . ....... @%&&&                          " + "");
		System.out.println("        , (.@%&        @&.. ..... .. ... .  ...  ..&&@                          " + "");
		System.out.println("     .%%%%%%%%&(      *%..........@@@@@@@&...... . **&@                         " + "");
		System.out.println("      @%%%%%%%%&&&    %@      &@@(. ..  . ../@@, .***@&                         " + "");
		System.out.println("      %%%%%%%%%%%&&&@ %@.  ..... ....... ....... ... %&                         " + "");
		System.out.println("      %%%%%%%%%%%%@@ ... ... ... ... ... ... ... ... .../@                      " + "");
		System.out.println("      %%%%%%%%%@#( ..... ....... ....... ....... ........*/%@@                  " + "");
		System.out.println("      &%%%%%%@%, . . . . . . . . . . . . . . . . . . . .****%%&%%.              " + "");
		System.out.println("       &%%%@%%.. ....... ....... ....... ....... .......****&%%@%%%@            " + "");
		System.out.println("        %%%%%@.. ... ... ... ... ... ... ... ... ... .*****,%%%%@%%%%%          " + "");
		System.out.println("         %%%%%%. ....... ....... ....... ....... ...******@#%%%%%%%%%%%&        " + "");
		System.out.println("        %%%%%%%%@, . ..(@&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%@%%%%%%%       " + "");
		System.out.println("       /%%%@&%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%@  @%@%%%%%%%%      " + "");
		System.out.println("       @%%% .,@@@@@%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%@...   ,..@%%&& %%&(@    " + "");
		System.out.println("   *,%@@............. @%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%.............  .,&&&*      " + "");
		System.out.println("    @  @...............%&&%%%%%%%%%%%%%%%%%%%%%%%%%*................../ #       " + "");
		System.out.println("    ..........**********@&&&%%%%%%%%%%%%%%%%%%%%%%@.. .********. ......         " + "");
		System.out.println("   *.......***&(((((@***@&&&&%%%%%%%%%%%%%%%%%%%%%@.*@((((((((@***...@,,,*      " + "");
		System.out.println("  /. @....**(((((((((**&&&&&&&&&&&&&&&&&&&&&&&&&&&@**@(((((((((***,...,         " + "");
		System.out.println("    # ... *&(((((((#**@&&&&&&&&&&&&&&&&&&&&&&&&&&&&&**%((((((#/***...           " + "");
		System.out.println("      @....**@%&@**&*                                 @********* .@             " + "");
	}

	public static void snorlaxespladasprite() {// Sprite de snorlax de espaldas
		System.out.println("                          %%%%%%%#(%%%%%%%%%%%%%%%,    /%%%%*                   " + "");
		System.out.println("                         %%//////%///////////////(%%%%#////%%                   " + "");
		System.out.println("                         %/////////////////////////////////%%%                  " + "");
		System.out.println("                       %%////////////////////////////////////%%#                " + "");
		System.out.println("                     (%///////////////////////////////////////%%                " + "");
		System.out.println("                     %%//////////////////////////////////////(%                 " + "");
		System.out.println("                    (%%%//////////////////////////////////////%%%               " + "");
		System.out.println("               .%%%(////////////////////////////////////////////%%%             " + "");
		System.out.println("            %%%(//////////////////////////////////////////////////%%%           " + "");
		System.out.println("          %%////////////////////////////////////////////////////////%%%         " + "");
		System.out.println("        %%///////(///////////////////////////////////////////////////(%%        " + "");
		System.out.println("       %///////%///////////////////////////////////////////////////////%%*      " + "");
		System.out.println("     ,#//////#%/////////////////////////////////////////////////////////%%      " + "");
		System.out.println("    . ##////##//////////////////////////////////////////////////////////#%%     " + "");
		System.out.println("        %%###////////////////////////////////////////////////////////////%%     " + "");
		System.out.println("          %%%////////////////////////////////////////////////////////////%%     " + "");
		System.out.println("           #%%///////////////////////////////////////////////////////////%%     " + "");
		System.out.println("        ,  .%#(//////////////////////////////////////////////////////////%%     " + "");
		System.out.println("       % %#%###/////////////////////////////////////////////////////////#%      " + "");
		System.out.println("       %(.....%#////////////////////////////////////////////////////////%,      " + "");
		System.out.println("    % .## .....///////////////////////////////////////////////////////(%%       " + "");
		System.out.println("      (%%,......*////////////////////////////////////////////////////%%         " + "");
		System.out.println("        (%%..... ../////////////////////////////////////////////(/%%%           " + "");
		System.out.println("          ,%%%,... ..%%%%#(////////////////////////////(##%%%%%%#               " + "");
		System.out.println("              /%%%%%%    ,(%%%%%%%%%%%%%%%%%%%%%%%%%%%%(.                       " + "");
	}

	public static void dragonitesprite() {// Sprite de dragonite
		System.out.println("                                           ,.*(                                 " + "");
		System.out.println("                                       #,***#,                                  " + "");
		System.out.println("                   ,,@               ,@////@*                                   " + "");
		System.out.println("                  ,,,,,(           ,%(/////(                                    " + "");
		System.out.println("           ,*    % ,,,,,,,        ,#(#%////(                                    " + "");
		System.out.println("          (    /(..****&.,(      (/////(/                            %*****(    " + "");
		System.out.println("         , *(** (/ ///////(     #((((                            /******((%%(#&(" + "");
		System.out.println("       #,,,,,,,,*%      %*,,,,//#&,,,,,,,,,,                   /*****&(*        " + "");
		System.out.println("  /,,,,,,,,,@ ,,********,,,,,,,,,,,,,,,,,,,,,,//(             *******(          " + "");
		System.out.println("  ,,,,,,,,,,,,,,*********,,,,,,,,,,,,,,,,,,,,,,*////////************@           " + "");
		System.out.println("   ,,,,,,,,,,,,,**********,,,,,,,,,,,,,,,,,,,,,,,///////***********,(           " + "");
		System.out.println("       #&(((((**(/*********,,,,,,,,,,,,,,,,,,,,,,,///////*******,&%(            " + "");
		System.out.println("        #***&*(   ,&,%*******,,,.,******,,,,,,,,,,***#%#**,,,,%((               " + "");
		System.out.println("          ((/     //@,,,,(,&/(,,.,,,//#,.,,,,,,,,,******/%((                    " + "");
		System.out.println("                 /////%@,,****,,,,,,**,,,,,,,,,,,,(                             " + "");
		System.out.println("                 ***/// ////&*/,,,,,,,,,,,,,,,,,,(                              " + "");
		System.out.println("                 ******(***////,,,,,,@,,,,,,,,,*/(                              " + "");
		System.out.println("                 /*.***( ***///,&,,,, //******///@                              " + "");
		System.out.println("                  .(  &     /////(@(         //**@                              " + "");
		System.out.println("                           #///#(           /,,,,(                              " + "");
		System.out.println("                          ,***&(            ,&, ,(                              " + "");
		System.out.println("                          .(.(              ( . (                               " + "");
		System.out.println("                          &(,                                                   " + "");
	}

	public static void dragoniteespladasprite() {// Sprite de dragonite de espaldas
		System.out.println("                             ,,                 ,.                              " + "");
		System.out.println("                              .,                 .,                             " + "");
		System.out.println("                                    /( ,,,.(/ ,,,                               " + "");
		System.out.println("                                   *,,,,,,,,**                                  " + "");
		System.out.println("                                  *,,,,,,,,***/                                 " + "");
		System.out.println("                                  ,,,,,,,,,,**/                                 " + "");
		System.out.println("                                  ,,,,,,,,,****                                 " + "");
		System.out.println("                                  ,,,,,,,,*,***                                 " + "");
		System.out.println("                                  ,,,,,,,,,****                                 " + "");
		System.out.println("                               ,,**,,,,,,,,,***/ ,,                             " + "");
		System.out.println("                              ,*****.,,*****,**,,*,,                            " + "");
		System.out.println("                            *,*******/////((*,,,,,,,,                           " + "");
		System.out.println("                        ,***,*******,//**/((,,,,,,,,,**                         " + "");
		System.out.println("                        ,***,*******,//**/((,,,,,,,,,**                         " + "");
		System.out.println("                     ,,****,**,,,,,,*/*****///*,,,,.,,/***                      " + "");
		System.out.println("                    ,,,*//*,*,,,,,,,,******///*,***,*,*****                     " + "");
		System.out.println("                   ,      ,,,,,,,,,,,****,,**/**,****,,,//*                     " + "");
		System.out.println("                        *,,,,,,,,,,,,,*,,,,,,*/********/                        " + "");
		System.out.println("                       *,,,,,,,,,,,,,,,,,,,,,,*********//                       " + "");
		System.out.println("                      ,,,,,,,,,,,,,,,,,,,,,,,*,,***,,****                       " + "");
		System.out.println("                      ,,,,,,,,,,,,,,,,,,,.,**************/                      " + "");
		System.out.println("                      ,,,,,,,,,,,,,,,,*,.,,***************                      " + "");
		System.out.println("                      *,,,,,,**,,,,***,*,,,,/**/*********/                      " + "");
		System.out.println("                     ***,,,,***,,,**,,,,,,,,,,************/                     " + "");
		System.out.println("                      ,,,,,**///,,,,,,*,#***#,***/**/*****                      " + "");
		System.out.println("                        *,****//(,,,,*,(,,,,,,,**/*/****.                       " + "");
		System.out.println("                             .     .,*,*/**#**,.                                " + "");

	}

	public static int ascuasdano() { // Todas las siguientes funciones son el dano que los ataques que son llamadas
										// en las primeras funciones de pokemonvspokemon();
		// son todas iguales y solo cambia la cantidad de dano asi que no voy a comentar
		// m�s de una para evitar rebundancia
		int dano = 40; // cantidad de dano
		return dano; // Devuelve la cantidad
	}

	public static int lanzallamasdano() {
		int dano = 80;
		return dano;
	}

	public static int pistolaaguadano() {
		int dano = 40;
		return dano;
	}

	public static int hidrobombadano() {
		int dano = 80;
		return dano;
	}

	public static int hojaafiladadano() {
		int dano = 40;
		return dano;
	}

	public static int bombagermendano() {
		int dano = 80;
		return dano;
	}

	public static int lenguetazodano() {
		int dano = 40;
		return dano;
	}

	public static int bolasombradano() {
		int dano = 80;
		return dano;
	}

	public static int bombalododano() {
		int dano = 40;
		return dano;
	}

	public static int venenoxdano() {
		int dano = 40;
		return dano;
	}

	public static int placajedano() {
		int dano = 40;
		return dano;
	}

	public static int aranazodano() {
		int dano = 40;
		return dano;
	}

	public static int golpecuerpodano() {
		int dano = 80;
		return dano;
	}

	public static int golpeodano() {
		int dano = 80;
		return dano;
	}

	public static int ataquealadano() {
		int dano = 40;
		return dano;
	}

	public static int vuelodano() {
		int dano = 80;
		return dano;
	}

	public static int alientodragondano() {
		int dano = 40;
		return dano;
	}

	public static int garradragondano() {
		int dano = 80;
		return dano;
	}

	public static double menucombatecharizardj1() { // Estas funciones son el menu de cada pokemon, y como en todas las
													// funciones anteriores solo comentare la primera.
		uiataquescharizard(); // LLama a la ui del pokemon donde se muestran los ataques
		Scanner reader = new Scanner(System.in);
		int opcion = 0;
		double dano = 0;
		opcion = reader.nextInt();
		if (opcion == 1) {
			ascuassprite();
			dano = ascuasdano();
		} // Si eligen 1 se llamara a la sprite del ataque elegido y su dano
		if (opcion == 2) {
			lanzallamassprite();
			dano = lanzallamasdano();
		}
		if (opcion == 3) {
			ataquealasprite();
			dano = ataquealadano();
		}
		if (opcion == 4) {
			vuelosprite();
			dano = vuelodano();
		}
		return dano; // Se devolvera el dano
	}

	public static double menucombateblastoisej1() {
		uiataquesagua();
		Scanner reader = new Scanner(System.in);
		int opcion = 0;
		double dano = 0;
		opcion = reader.nextInt();
		if (opcion == 1) {
			pistolaaguasprite();
			dano = pistolaaguadano();
		}
		if (opcion == 2) {
			hidrobombasprite();
			dano = hidrobombadano();
		}
		if (opcion == 3) {
			placajesprite();
			dano = placajedano();
		}
		if (opcion == 4) {
			aranazosprite();
			dano = aranazodano();
		}
		return dano;
	}

	public static double menucombatevenusaurj1() {
		uiataquesvenusaur();
		Scanner reader = new Scanner(System.in);
		int opcion = 0;
		double dano = 0;
		opcion = reader.nextInt();
		if (opcion == 1) {
			hojaagudasprite();
			dano = hojaafiladadano();
		}
		if (opcion == 2) {
			bombagermensprite();
			dano = bombagermendano();
		}
		if (opcion == 3) {
			bombalodosprite();
			dano = bombalododano();
		}
		if (opcion == 4) {
			venenoxsprite();
			dano = venenoxdano();
		}
		return dano;
	}

	public static double menucombategengarj1() {
		uiataquesgengar();
		Scanner reader = new Scanner(System.in);
		int opcion = 0;
		double dano = 0;
		opcion = reader.nextInt();
		if (opcion == 1) {
			lenguetazosprite();
			dano = lenguetazodano();
		}
		if (opcion == 2) {
			bolasombrasprite();
			dano = bolasombradano();
		}
		if (opcion == 3) {
			bombalodosprite();
			dano = bombalododano();
		}
		if (opcion == 4) {
			venenoxsprite();
			dano = venenoxdano();
		}
		return dano;
	}

	public static double menucombatesnorlaxj1() {
		uiataquesnormal();
		Scanner reader = new Scanner(System.in);
		int opcion = 0;
		double dano = 0;
		opcion = reader.nextInt();
		if (opcion == 1) {
			placajesprite();
			dano = placajedano();
		}
		if (opcion == 2) {
			aranazosprite();
			dano = aranazodano();
		}
		if (opcion == 3) {
			golpecuerposprite();
			dano = golpecuerpodano();
		}
		if (opcion == 4) {
			golpeosprite();
			dano = golpeodano();
		}
		return dano;
	}

	public static double menucombatedragonitej1() {
		uiataquesdragon();
		Scanner reader = new Scanner(System.in);
		int opcion = 0;
		double dano = 0;
		opcion = reader.nextInt();
		if (opcion == 1) {
			alientodragonsprite();
			dano = alientodragondano();
		}
		if (opcion == 2) {
			garradragonsprite();
			dano = garradragondano();
		}
		if (opcion == 3) {
			ataquealasprite();
			dano = ataquealadano();
		}
		if (opcion == 4) {
			vuelosprite();
			dano = vuelodano();
		}
		return dano;
	}

	public static double charizardia() {
		Random random = new Random();
		int opcion = 0;
		double dano = 0;
		opcion = random.nextInt(4) + 1;
		if (opcion == 1) {
			ascuassprite();
			dano = ascuasdano();
		}
		if (opcion == 2) {
			lanzallamassprite();
			dano = lanzallamasdano();
		}
		if (opcion == 3) {
			ataquealasprite();
			dano = ataquealadano();
		}
		if (opcion == 4) {
			vuelosprite();
			dano = vuelodano();
		}
		return dano;
	}

	public static double blastoiseia() {
		Random random = new Random();
		int opcion = 0;
		double dano = 0;
		opcion = random.nextInt(4) + 1;
		if (opcion == 1) {
			pistolaaguasprite();
			dano = pistolaaguadano();
		}
		if (opcion == 2) {
			hidrobombasprite();
			dano = hidrobombadano();
		}
		if (opcion == 3) {
			placajesprite();
			dano = placajedano();
		}
		if (opcion == 4) {
			aranazosprite();
			dano = aranazodano();
		}
		return dano;
	}

	public static double venusauria() {
		Random random = new Random();
		int opcion = 0;
		double dano = 0;
		opcion = random.nextInt(4) + 1;
		if (opcion == 1) {
			hojaagudasprite();
			dano = hojaafiladadano();
		}
		if (opcion == 2) {
			bombagermensprite();
			dano = bombagermendano();
		}
		if (opcion == 3) {
			bombalodosprite();
			dano = bombalododano();
		}
		if (opcion == 4) {
			venenoxsprite();
			dano = venenoxdano();
		}
		return dano;
	}

	public static double gengaria() {
		Random random = new Random();
		int opcion = 0;
		double dano = 0;
		opcion = random.nextInt(4) + 1;
		if (opcion == 1) {
			lenguetazosprite();
			dano = lenguetazodano();
		}
		if (opcion == 2) {
			bolasombrasprite();
			dano = bolasombradano();
		}
		if (opcion == 3) {
			bombalodosprite();
			dano = bombalododano();
		}
		if (opcion == 4) {
			venenoxsprite();
			dano = venenoxdano();
		}
		return dano;
	}

	public static double snorlaxia() {
		Random random = new Random();
		int opcion = 0;
		double dano = 0;
		opcion = random.nextInt(4) + 1;
		if (opcion == 1) {
			placajesprite();
			dano = placajedano();
		}
		if (opcion == 2) {
			aranazosprite();
			dano = aranazodano();
		}
		if (opcion == 3) {
			golpecuerposprite();
			dano = golpecuerpodano();
		}
		if (opcion == 4) {
			golpeosprite();
			dano = golpeodano();
		}
		return dano;
	}

	public static double dragoniteia() {
		Random random = new Random();
		int opcion = 0;
		double dano = 0;
		opcion = random.nextInt(4) + 1;
		if (opcion == 1) {
			alientodragonsprite();
			dano = alientodragondano();
		}
		if (opcion == 2) {
			garradragonsprite();
			dano = garradragondano();
		}
		if (opcion == 3) {
			ataquealasprite();
			dano = ataquealadano();
		}
		if (opcion == 4) {
			vuelosprite();
			dano = vuelodano();
		}
		return dano;
	}

	public static String[] ataquescharizard() { // Esta funcion es la que muestra los ataques en las ui de los pokemon,
												// como antes solo comentare la primera
		String ataques[] = new String[4];
		ataques[0] = "Ascuas"; // El nombre del ataque
		ataques[1] = "Lanzallamas";// El nombre del ataque
		ataques[2] = "Ataque ala";// El nombre del ataque
		ataques[3] = "Vuelo";// El nombre del ataque
		return ataques; // Mandara un string con los nombres de los ataques a la ui para que se muestren
						// en el menu.
	}

	public static String[] ataquestipoagua() {
		String ataques[] = new String[4];
		ataques[0] = "Pistola agua";
		ataques[1] = "Hidrobomba";
		ataques[2] = "Placaje";
		ataques[3] = "Aranazo";
		return ataques;
	}

	public static String[] ataquesvenusaur() {
		String ataques[] = new String[4];
		ataques[0] = "Hoja Afilada";
		ataques[1] = "Bomba Germen";
		ataques[2] = "Bomba Lodo";
		ataques[3] = "Veneno X";
		return ataques;
	}

	public static String[] ataquesgengar() {
		String ataques[] = new String[4];
		ataques[0] = "Lenguetazo";
		ataques[1] = "Bola Sombra";
		ataques[2] = "Bomba lodo";
		ataques[3] = "Veneno X";
		return ataques;
	}

	public static String[] ataquestiponormal() {
		String ataques[] = new String[4];
		ataques[0] = "Aranazo";
		ataques[1] = "Placaje";
		ataques[2] = "Golpe Cuerpo";
		ataques[3] = "Golpeo";
		return ataques;
	}

	public static String[] ataquestipodragon() {
		String ataques[] = new String[4];
		ataques[0] = "Aliento Dragon";
		ataques[1] = "Garra Dragon";
		ataques[2] = "Ataque ala";
		ataques[3] = "Vuelo";
		return ataques;
	}

}
