import java.util.Scanner;

public class BatallaNaval {

	public static void play() {

		//INICIALIZACION DE VARIABLES
		Scanner reader = new Scanner(System.in);
		int tablero[][] = new int [10][10];
		boolean columnaCero = false;
		boolean filaCero = false;
		int contador = 0;
		int tamano = 0;
		int fila = 0;
		int columna = 0;
		int dificultad = 0;

		//INTRODUCCION DE DATOS
		System.out.println("Escoge la dificultad");
		System.out.println("0) 6x6\n1) 8x8\n2) 10x10");
		dificultad = reader.nextInt();
		if (dificultad < 0 || dificultad > 3) {
			System.out.println("Has introducido un valor err�neo, se escoger� el tablero de 6x6");
			try {Thread.sleep(2000);} catch(InterruptedException e) {}
		}
		System.out.println("\n\n\n");
		//SWITCH PARA DETERMINAR DIFICULTAD
		switch (dificultad) {
		case 0:
			tamano = 6;
			break;
		case 1:
			tamano = 8;
			break;
		case 2:
			tamano = 10;
			break;
		default:
			tamano = 10;
			break;
		}

		tablero = new int[tamano][tamano];
		//LLAMADA DE LA FUNCI�N DANDO COMO DATO EL VALOR DE LA ARRAY TABLERO
		imprimirTablero(tablero);

		System.out.println("\n\n\nAhora escoge la posicion de tu barco 1. Es un 3x1");
		System.out.println("\n\nDebes poner la primera posici�n.");

		do { //BUCLE HASTA QUE SE PONGA EL PRIMER BARCO
			do { //CONTROL DE DATOS
				System.out.println("\n\nIntroduce la columna");
				columna = reader.nextInt();
				if (columna == 0) {
					columnaCero = true;
				}
			} while (columna < 0 || columna > tamano);
			do { //CONTROL DE DATOS
				System.out.println("Introduce la fila");
				fila = reader.nextInt();
				if (fila == 0) {
					filaCero = true;
				}
				System.out.println("\n");
			} while (fila < 0 || fila > tamano);

			if (contador == 0) {
				barcosJugadorUno(fila, columna, tablero);
			} else if (contador>0) {
				posicionCeros(fila, columna, tablero, columnaCero, filaCero, contador);
				imprimirTablero(tablero);
			}
			contador++;
		} while(contador < 3);
	}

	//FUNCION - Si la fila o columna estan cerca de los bordes 0
	public static void posicionCeros (int a, int b, int [][] d, boolean cc, boolean fc, int cont) {

		if(cc == true && fc == false && d[a][b] == 0) { //SI Columna es 0
			if(d[a][b - 1] != 0  || d[a][b + 1] != 0) {
				d[a][b] = cont + 1;
			}  else if (d[a][b - 1] != 0 || d[a][b + 1] != 0 || d[a + 1][b - 1] != 0 || d[a + 1][b + 1] != 0 ) {
				d[a][b] = cont + 1;
			}
		} else if (fc == true && cc == false && d[a][b] == 0) { //SI Fila es 0
			if (d[a - 1][b] != 0 || d[a - 1][b + 1] != 0 || d[a + 1][b] != 0 || d[a + 1][b + 1] != 0 ) {
				d[a][b] = cont + 1;
			} else if (d[a - 1][b] != 0 || d[a + 1][b] != 0) {
				d[a][b] = cont + 1;
			}
		} else if(fc == true && cc == true && d[a][b] == 0 ) {
			d[a][b] = cont + 1;
		} if (d[a][b] == 0 && cc == false && fc == false) {
			if(d[a][b - 1] != 0  || d[a][b + 1] != 0) {
				d[a][b] = cont + 1;
			} else if (d[a - 1][b] != 0 || d[a + 1][b] == 0) {
				d[a][b] = cont + 1;
			} else if (d[a - 1][b - 1] != 0 || d[a - 1][b + 1] != 0 || d[a + 1][b - 1] != 0 || d[a + 1][b + 1] != 0 ) {
				d[a][b] = cont + 1;
			} else {
				System.out.println("No has puesto la posici�n cerca del barco");
			}
		} else if (d[a][b] != 0) {
			System.out.println("Esta posicion esta cogida");
		}
	}



	//FUNCION - Print de la tabla para el usuario
	public static void imprimirTablero (int [][] a) {

		//BUCLE FOR ANIDADO PARA EL PRINT
		for (int i = 0; i < a.length; i++) { //HORIZONTAL
			for (int j = 0; j < a.length; j++) { //VERTICAL
				System.out.print(a[i][j] + " "); //SYSO DE LA POSICION I Y J, ESPACIO ENTRE UNA Y OTRA
			} //PRINT LATERAL DERECHO DE N�MEROS PARA COORDENADAS
			System.out.print(i);
			System.out.println(); //SALTO DE LINEA PARA EL SIGUIENTE PRINT DE LA MATRIZ
		} //PRINT DE LOS N�MEROS INFERIORES PARA COORDENADAS
		for (int i = 0; i < a.length; i++) {
			System.out.print(i + " ");
		}
	}

	//FUNCION - El usuario introduce los barcos y el programa lo printa
	public static void barcosJugadorUno (int a, int b, int [][] d) {

		for (int i = 0; i < d.length; i++) { 
			for (int j = 0; j < d.length; j++) { 
				if (d[i][j] == 0) { //SI EL TABLERO ES 0, LA COLUMNA A y FILA B PASARA DE 0 A 1
					d[a][b] = 1;
				}
				System.out.print(d[i][j] + " "); //SYSO DE LA POSICION I Y J, ESPACIO ENTRE UNA Y OTRA
			} //PRINT LATERAL DERECHO DE N�MEROS PARA COORDENADAS
			System.out.print(i +"\n");
		} //PRINT DE LOS N�MEROS INFERIORES PARA COORDENADAS
		for (int i = 0; i < d.length; i++) {
			System.out.print(i + " ");
		}

	}

}



